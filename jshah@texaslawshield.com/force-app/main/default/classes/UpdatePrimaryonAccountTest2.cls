// This test class is for the Apex Class UpdatePrimaryOnAccount
@isTest
public class UpdatePrimaryonAccountTest2 {


    //Method for testing updateAccounts method with no actual Account data.
    static testMethod void testUpdateAccountsNegative(){
        
        Set<Id> setId = new Set<Id>{'0012200000KJclB'};
        Test.startTest();
        List<Database.SaveResult> lstSaveResult = UpdatePrimaryOnAccount.updateAccounts(setId);
        Test.stopTest();
        
        System.assertEquals(lstSaveResult,null);
    }

    //Method for testing updateAccounts method with  Account data.
    static testMethod void testUpdateAccountsPositive(){
        List<Account> lstAccount = TestDataFactory.getAccounts(100, true);
        Set<Id> setId = new Set<Id>{};
        for (Account objAccount : lstAccount) {
            setId.add(objAccount.Id);
        }
        Test.startTest();
            List<Database.SaveResult> lstSaveResult = UpdatePrimaryOnAccount.updateAccounts(setId);
        Test.stopTest();
        for (Database.SaveResult objSaveResult : lstSaveResult) {
            System.assertEquals(objSaveResult.isSuccess(),true);
        }
    }
    
    @IsTest
    static void testGetallSubscriptionsPositive(){
        
        List<Zuora__SubscriptionProductCharge__c> lstSubscriptionProductCharge
                                    =  TestDataFactory.getZuoraSubscriptionProductCharge(10,true);
        Test.startTest();
            UpdatePrimaryOnAccount.getallSubscriptions(lstSubscriptionProductCharge);
        Test.stopTest();
        Set<Id> setId = new Set<Id>();
        for (Zuora__SubscriptionProductCharge__c objProductCharge: lstSubscriptionProductCharge) {
            setId.add(objProductCharge.Zuora__Account__c);
        }
        List<Account> lstAccount = new List<Account>();
        lstAccount = [ SELECT Primary__c FROM Account WHERE Id IN :setId ];

        for (Account objAccount : lstAccount) {
            System.assertEquals(objAccount.Primary__c, true);
        }

    }

    @isTest
    static void testGetallSubscriptionsNegative(){
        
        List<Zuora__SubscriptionProductCharge__c> lstSubscriptionProductCharge
                                    =  TestDataFactory.getZuoraSubscriptionProductCharge(1,true);
        for (Zuora__SubscriptionProductCharge__c obj : lstSubscriptionProductCharge) {
            obj.Zuora__RatePlanName__c = 'Couple Plan';
        }
        update lstSubscriptionProductCharge;
        Test.startTest();
            UpdatePrimaryOnAccount.getallSubscriptions(lstSubscriptionProductCharge);
        Test.stopTest();
        Set<Id> setId = new Set<Id>();
        for (Zuora__SubscriptionProductCharge__c objProductCharge: lstSubscriptionProductCharge) {
            setId.add(objProductCharge.Zuora__Account__c);
        }
        List<Account> lstAccount = new List<Account>();
        lstAccount = [ SELECT Primary__c FROM Account WHERE Id IN :setId ];

        for (Account objAccount : lstAccount) {
            System.assertEquals(objAccount.Primary__c, false);
        }

    }
    
    
    static testMethod void testGetallSubscriptionsforUpdatePositive(){

        Map <Id, Zuora__SubscriptionProductCharge__c> mapOldSubscriptions 
                                                = new Map<Id,Zuora__SubscriptionProductCharge__c>();
        List<Zuora__SubscriptionProductCharge__c> lstSubscriptionProductCharge
                                    = TestDataFactory.getZuoraSubscriptionProductCharge(10,true);
        mapOldSubscriptions.putAll(lstSubscriptionProductCharge);
        Test.startTest();
            UpdatePrimaryOnAccount.getallSubscriptionsforUpdate
                                                (lstSubscriptionProductCharge, mapOldSubscriptions);
        Test.stopTest();
        Set<Id> setId = new Set<Id>();
        for (Zuora__SubscriptionProductCharge__c objProductCharge: lstSubscriptionProductCharge) {
            setId.add(objProductCharge.Zuora__Account__c);
        }
        List<Account> lstAccount = new List<Account>();
        lstAccount = [ SELECT Primary__c FROM Account WHERE Id IN :setId ];

        for (Account objAccount : lstAccount) {
            System.assertEquals(objAccount.Primary__c, true);
        }
        
    }
    static testMethod void testGetallSubscriptionsforUpdateNegative(){

        Map <Id, Zuora__SubscriptionProductCharge__c> mapOldSubscriptions 
                                                = new Map<Id,Zuora__SubscriptionProductCharge__c>();
        List<Zuora__SubscriptionProductCharge__c> lstSubscriptionProductCharge
                                    = TestDataFactory.getZuoraSubscriptionProductCharge(10,true);
        for (Zuora__SubscriptionProductCharge__c obj : lstSubscriptionProductCharge) {
            obj.Zuora__RatePlanName__c = 'Couple Plan';
        }
        update lstSubscriptionProductCharge;
        mapOldSubscriptions.putAll(lstSubscriptionProductCharge);
        Test.startTest();
            UpdatePrimaryOnAccount.getallSubscriptionsforUpdate
                                                (lstSubscriptionProductCharge, mapOldSubscriptions);
        Test.stopTest();
        Set<Id> setId = new Set<Id>();
        for (Zuora__SubscriptionProductCharge__c objProductCharge: lstSubscriptionProductCharge) {
            setId.add(objProductCharge.Zuora__Account__c);
        }
        List<Account> lstAccount = new List<Account>();
        lstAccount = [ SELECT Primary__c FROM Account WHERE Id IN :setId ];

        for (Account objAccount : lstAccount) {
            System.assertEquals(objAccount.Primary__c, false);
        }
        
    }

    
    
}