/*-----------------------------------------------------------------------------------------------------------------------------
*   Author:         Strategic Growth, Inc. (www.strategicgrowthinc.com)
*   Description:    This class is used to test the sgi_EventRequest_Controller controller class and EventRegister VF page
*
*   History:
*       Date            Developer       Comments
*       ----------      --------------  ---------------------------------------------------------------------------
*       4/15/2016       CB              - Initial development
*		7/27/2016		CB				- Updates to support requested changes to the front end as well as moving the page to use the 
*											Special Event object instead of the Event Request Template Object
*		11/11/2016		CB				- Change to run off the Case object directly since not all users have license to the Special Event Linvio object
*
/*-----------------------------------------------------------------------------------------------------------------------------*/

@isTest
private class sgi_EventRequest_Controller_Test 
{
    private static DateTime CurrentDateTime = DateTime.newInstance(Date.today(), Time.newInstance(1, 0, 0, 0));
    
	// Method used to create a new Account
	private static Account createNewAccount(Account objA)
	{
		if (objA == null)
		{
			objA = new Account();
		}
		
		objA.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Facility (Customer)').getRecordTypeId();
		objA.Name = 'SGI Testing New Account for Test Class';
		objA.Type = 'Facility-Customer';
		objA.Facility_Type__c = 'Organization';
		objA.AccountType__c = 'Facility';
		
		insert objA;
		
		return objA;
	}
	
	// Method used to create a new Contact associated to an Account
	private static Contact createNewContact(Contact objC, Id AccountId)
	{
		if (objC == null)
		{
			objC = new Contact();
		}
		
		objC.RecordTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Standard').getRecordTypeId();
		objC.FirstName = 'SGI';
		objC.LastName = 'Testing';
		objC.AccountId = AccountId;
		objC.Level__c = 'Employee';
		objC.Functional_Area__c = 'Management';
		objC.Phone = '5553234455';
		objC.Email = 'SGITesting1234@SGITesting1234.com';
		
		insert objC;
		
		return objC;
	}
	
	static testMethod void testWorkshop() 
	{
		// Create an Account with a Contact to use for Host Facility and Contact info
		Account objA = createNewAccount(new Account());
		Contact objC = createNewContact(new Contact(), objA.Id);
		
 		Case objCase = new Case();
        
		ApexPages.StandardController sc = new ApexPages.StandardController(objCase);
		
		// Create a reference to the extension controller
		sgi_EventRequest_Controller cntrl = new sgi_EventRequest_Controller(sc);
		
		// Create an instance of the page
		PageReference pr = Page.EventRequest;
		
		Test.setCurrentPage(pr);
		
		// Run through some of the public variables and make sure they are set correctly
		system.assertNotEquals(null, cntrl.strReqName, '*** sgi_EventRequest_Controller_Test - strReqName Missing');
		system.assertNotEquals(null, cntrl.strReqEmail, '*** sgi_EventRequest_Controller_Test - strReqEmail Missing');
		
        list<SelectOption> lstEventTypes = cntrl.lstEventTypes;
        
		// Fill out the fields on the page
		cntrl.strEventType = 'Workshop';
		cntrl.objCase.Date_Time_Event__c = CurrentDateTime;
		cntrl.objCase.Event_End_Time__c = CurrentDateTime.addHours(2);
		cntrl.objCase.Event_Time_Zone__c = '(GMT-05:00) Central Daylight Time (America/Chicago)';
        cntrl.objCase.State_Region__c = '1.2.3 SW Houston';
		cntrl.objCase.Host_Facility__c = objA.Id;
        // Load the Contacts
        list<SelectOption> lstContacts = cntrl.lstFacilityContacts;
        // Pick a Contact
		cntrl.strFacilityContact = objC.Id;
		cntrl.objCase.Additional_Details__c = 'Testing additional details';
		
		// Add Attachments
		List<Attachment> newAtts = cntrl.newAttachments;
		newAtts[0].Name='Unit Test 1';
		newAtts[0].Description='Unit Test 1';
		newAtts[0].Body=Blob.valueOf('Unit Test 1');

		newAtts[2].Name='Unit Test 2';
		newAtts[2].Description='Unit Test 2';
		newAtts[2].Body=Blob.valueOf('Unit Test 2');
		
		// Save the request
		cntrl.SaveRequest();
		
		// Make sure a new Case was created
		system.assertNotEquals(cntrl.objCase.Id, null, '*** sgi_EventRequest_Controller_Test - Case Not Created');
	}
	
	static testMethod void testSeminar() 
	{
		// Create an Account with a Contact to use for Host Facility and Contact info
		Account objA = createNewAccount(new Account());
		Contact objC = createNewContact(new Contact(), objA.Id);
		
		// Create an Case record for the standard controller
		Case objCase = new Case();
		
		ApexPages.StandardController sc = new ApexPages.StandardController(objCase);
		
		// Create a reference to the extension controller
		sgi_EventRequest_Controller cntrl = new sgi_EventRequest_Controller(sc);
		
		// Create an instance of the page
		PageReference pr = Page.EventRequest;
		
		Test.setCurrentPage(pr);
		
		// Run through some of the public variables and make sure they are set correctly
		system.assertNotEquals(null, cntrl.strReqName, '*** sgi_EventRequest_Controller_Test - strReqName Missing');
		system.assertNotEquals(null, cntrl.strReqEmail, '*** sgi_EventRequest_Controller_Test - strReqEmail Missing');
		
        string strCNumber = cntrl.strCaseNumber;
        string strCType = cntrl.strCaseType;
        
		// Fill out the fields on the page
		cntrl.strEventType = 'Seminar';
        cntrl.objCase.Date_Time_Event__c = CurrentDateTime;
		cntrl.objCase.Event_End_Time__c = CurrentDateTime.addHours(2);
		cntrl.objCase.Event_Time_Zone__c = '(GMT-05:00) Central Daylight Time (America/Chicago)';
        cntrl.objCase.State_Region__c = '1.2.3 SW Houston';
		cntrl.objCase.Host_Facility__c = objA.Id;
        // Load the Contacts
        list<SelectOption> lstContacts = cntrl.lstFacilityContacts;
        // Pick a Contact
		cntrl.strFacilityContact = objC.Id;
		cntrl.objCase.Additional_Details__c = 'Testing additional details';
		
		// Add Venue Details
		cntrl.objCase.Venue_Name__c = 'Test Venue';
		cntrl.objCase.Venue_Street__c = 'Venue Street';
		cntrl.objCase.Venue_City__c = 'Venue City';
		cntrl.objCase.Venue_State__c = 'TX';
		cntrl.objCase.Venue_Postal_Code__c = '77002';
		cntrl.objCase.Venue_Contact_Name__c = 'Testing SGI';
		cntrl.objCase.Venue_Contact_Email__c = 'sgiVenueContact@testingVenue.com';
		cntrl.objCase.Venue_Contact_Phone__c = '5553214321';
		
		// Add Attachments
		List<Attachment> newAtts = cntrl.newAttachments;
		newAtts[0].Name='Unit Test 1';
		newAtts[0].Description='Unit Test 1';
		newAtts[0].Body=Blob.valueOf('Unit Test 1');

		newAtts[2].Name='Unit Test 2';
		newAtts[2].Description='Unit Test 2';
		newAtts[2].Body=Blob.valueOf('Unit Test 2');
		
		// Save the request
		cntrl.SaveRequest();
		
		// Make sure a new Case was created
		system.assertNotEquals(cntrl.objCase.Id, null, '*** sgi_EventRequest_Controller_Test - Case Not Created');
		system.assertNotEquals(cntrl.strCaseType, null, '*** sgi_EventRequest_Controller_Test - Case Type Not Created');
		system.assertNotEquals(cntrl.strCaseNumber, null, '*** sgi_EventRequest_Controller_Test - Case Type Not Created');
		
		cntrl.addMore();
	}
}