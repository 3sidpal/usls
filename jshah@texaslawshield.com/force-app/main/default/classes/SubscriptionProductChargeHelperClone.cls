/**
 * @ Author: Tharun G
 * @ Objects Referenced: Account, SubscriptionProductCharge__c
 * @ Description: This is refferenced in subscriptionProductChargeTrigger to calculate Member_Type__c in Account Object.
 * @ Version-1.0
 * @ Date: 06-30-2018
 */
public with sharing class SubscriptionProductChargeHelperClone {
    //
    private static final String ANNUAL  = 'Annual';
    private static final String DIAMOND = 'Diamond';
    private static final String MONTHLY = 'Monthly';
    private static final String OTHER   = 'Other';

    public static void getallsubscriptions(
        Set<Id> accountId,
        Boolean isdelete,
        Set<Id> deletingSubid
    ) {
        //In Helper
        system.debug('In Helper mrthod getallsubscriptions');
        List<Account> updateAccounts    = new List<Account>();
        Set<String> subscriptionNames   = new Set<String>();
        Map<Id, Set<String>> accSubName = new Map<Id, Set<String>>();

        for(Zuora__SubscriptionProductCharge__c allSubsItr : [
            SELECT Id
                 , Name
                 , Zuora__Account__c
                 , For_Test__c
              FROM Zuora__SubscriptionProductCharge__c
             WHERE Zuora__Account__c = :accountId
        ]) {
            system.debug('In FOR LOOP '+ allSubsItr);
            if(accSubName.containskey(allSubsItr.Zuora__Account__c)) {
                Set<String> subname= accSubName.get(allSubsItr.Zuora__Account__c);
                if(!deletingSubid.contains(allSubsItr.Id)) {
                    subname.add(allSubsItr.For_Test__c);
                    accSubName.put(allSubsItr.Zuora__Account__c, subname);
                }
            } else {
                if(!deletingSubid.contains(allSubsItr.Id)) {
                    accSubName.put(allSubsItr.Zuora__Account__c, new Set<String> {allSubsItr.For_Test__c} );
                }
            }
        } // End of for.

        String subscriptiontype;
        for(Account accItr : [
            SELECT Id
                 , Member_type__c
                 , (Select Id
                         , Name
                      FROM R00N40000001lGjTEAU
                   )
              FROM Account
             WHERE Id = :accountId
        ]) {
            if(accSubName.containskey(accItr.Id)) {
                Set<String> setsubs = accSubName.get(accItr.Id);
                if(setsubs.contains(ANNUAL)) {
                    accItr.member_type__c = ANNUAL;
                    updateAccounts.add(accItr);
                } else if(setsubs.contains(MONTHLY)) {
                    accItr.member_type__c = MONTHLY;
                    updateAccounts.add(accItr);
                } else if(setsubs.contains(DIAMOND)) {
                    accItr.member_type__c = DIAMOND;
                    updateAccounts.add(accItr);
                } else if(setsubs != null) {
                    accItr.member_type__c = OTHER;
                    updateAccounts.add(accItr);
                    system.debug('In Other Name LOOP');
                }
            }

            if(accItr.R00N40000001lGjTEAU.size() == 1 && isdelete) {
                accItr.member_type__c = null;
                updateAccounts.add(accItr);
                system.debug('In Delete Loop');
            }
        } // End of for.

        update updateAccounts;
        //
    }

    public static void populateAccountMaps(
        Set<Id> setAccountId,
        Map<Id, Account> accountMap,
        Map<Id, Zuora__SubscriptionProductCharge__c> newMap
    ) {
        //
        for (Zuora__SubscriptionProductCharge__c sub : newMap.values()) {
            if(sub.Zuora__Account__c != null) {
                setAccountId.add(sub.Zuora__Account__c);
            }
        }

        for(Account accItr : [
            SELECT Id
                 , Member_type__c
              FROM Account
             WHERE Id IN :setAccountId
        ]) {
            accountMap.put(accItr.Id, accItr);
        }
        //
    }
    //
}