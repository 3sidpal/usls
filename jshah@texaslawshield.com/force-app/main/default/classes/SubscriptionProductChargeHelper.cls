/*
 * @ Author: Tharun G
 * @ Objects Referenced: Account, SubscriptionProductCharge__c
 * @ Description: This is refferenced in subscriptionProductChargeTrigger to calculate Member_Type__c in Account Object.
 * @ Version-1.0
 * @ Date: 06-30-2018
    */

public class SubscriptionProductChargeHelper {

      //public static boolean isdeletefunctionality = false;
    public static void getallsubscriptions(set<id> accountid, boolean isdelete, set<id> deletingSubid)
    {
        //In Helper
        system.debug('In Helper mrthod getallsubscriptions');
        List<Account> updateAccounts=new List<Account>();
        set<string> subscriptionNames=new set<string>();
        Map<id,set<string>> accSubName= new Map<id,set<string>>();
        for(Zuora__SubscriptionProductCharge__c allsubs:[SELECT Id, Name, Zuora__Account__c,for_test__c FROM Zuora__SubscriptionProductCharge__c where Zuora__Account__c =: accountid])
        {
             system.debug('In FOR LOOP '+ allsubs);
            if(accSubName.containskey(allsubs.Zuora__Account__c))
            {
                set<string> subname= accSubName.get(allsubs.Zuora__Account__c);
                 if(!deletingSubid.contains(allsubs.id)){
                subname.add(allsubs.for_test__c);
                     accSubName.put(allsubs.Zuora__Account__c, subname);   }
            }
            else
            {
                 if(!deletingSubid.contains(allsubs.id)){
                accSubName.put(allsubs.Zuora__Account__c, new set<string> {allsubs.for_test__c} );
                 }}

            }

        string subscriptiontype;
        for(Account accs:[select id, member_type__c,(select id,name from R00N40000001lGjTEAU) from Account where id =: accountid])
        {
            if(accSubName.containskey(accs.id)){
                set<string> setsubs=accSubName.get(accs.id);
                if(setsubs.contains('Annual')){
                    accs.member_type__c='Annual';
                    updateAccounts.add(accs);
                }
                else if(setsubs.contains('Monthly')){
                     accs.member_type__c='Monthly';
                    updateAccounts.add(accs);
                }
                else if(setsubs.contains('Diamond')){
                     accs.member_type__c='Diamond';
                    updateAccounts.add(accs);
                }

                else if(setsubs != null){
                     accs.member_type__c='Other';
                    updateAccounts.add(accs);
                    system.debug('In Other Name LOOP');
                }
                }
            if(accs.R00N40000001lGjTEAU.size() ==1 && isdelete){
                 accs.member_type__c=null;
                    updateAccounts.add(accs);
                system.debug('In Delete Loop');
            }

            }
         Update updateAccounts;
        }


}