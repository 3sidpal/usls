@isTest
public class NumFormatBatchTest {

static testMethod void setup(){
delete [select id from Contact ];
Contact l = new Contact(lastname='jain' , phone='19623120179');
insert l;
}
static testMethod void setup1(){
delete [select id from Contact ];
Contact l = new Contact(lastname='jain' , phone='019623120179');
insert l;
}
static testMethod void setup2(){
delete [select id from Contact ];
Contact l = new Contact(lastname='jain' , phone='0019623120179');
insert l;
}



static testMethod void test_batch() {
setup();

Test.startTest();
NumFormatBatch n = new NumFormatBatch('Contact',new List<String>{'Phone'});
Database.executeBatch(n,200);
Test.stopTest();

Contact l = [select phone from Contact ];
String s = l.phone.replaceAll('[^0-9]','');
system.assertEquals(s.length(),10);
}

static testMethod void test_batch1() {
setup1();

Test.startTest();
NumFormatBatch n = new NumFormatBatch('Contact',new List<String>{'Phone'});
Database.executeBatch(n,200);
Test.stopTest();

Contact l = [select phone from Contact ];
String s = l.phone.replaceAll('[^0-9]','');
system.assertEquals(s.length(),10);
}

static testMethod void test_batch2() {
setup2();

Test.startTest();
NumFormatBatch n = new NumFormatBatch('Contact',new List<String>{'Phone'});
Database.executeBatch(n,200);
Test.stopTest();

Contact l = [select phone from Contact ];
String s = l.phone.replaceAll('[^0-9]','');
system.assertEquals(s.length(),10);
}

}