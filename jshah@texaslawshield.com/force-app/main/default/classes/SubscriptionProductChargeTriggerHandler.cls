/**
 * @ Author: Tharun G
 * @ Objects Referenced: Account, SubscriptionProductCharge__c
 * @ Description: This trigger is used to calculate Subsription type of member
 * @ @Annual/Monthly/Diamond/Other) based up on the name of the Subscription
 * @ Charge Name filed on  Zuora__SubscriptionProductCharge__c Object.
 *
 * @ Version     Modified Date      Modified By     Description
 * @ ------------------------------------------------------------
 * @     1.0     06-30-2018         Tharun G        Initial Draft
 * @     1.1     04-06-2019         Tharun G        This is calling another helper menthod to
 * @                                                update on Primary on Account
 * @     1.2     06-14-2019         Unmesha P       Modified code to fix the Apex Heap Limits issue
 * @     1.3     06-24-2019         Eternus         Code optimization to fix the Apex Heap Limits
 *                                                  issue
 */
public with sharing class SubscriptionProductChargeTriggerHandler implements ITriggerHandler {

    private static final String ANNUAL     = 'Annual';
    private static final String CLASS_NAME = 'subscriptionProductChargeTrigger';
    private static final String DIAMOND    = 'Diamond';
    private static final String MONTHLY    = 'Monthly';

    public Boolean isDisabled() {
        try {
            List<Trigger_Settings__mdt> metaDataList = [
                SELECT Id
                     , isActive__c
                  FROM Trigger_Settings__mdt
                 WHERE MasterLabel = :CLASS_NAME
            ];

            return !metaDataList.get(0).isActive__c;
        } catch(Exception e) {
            return true;
        }
    }

    public void beforeInsert(List<SObject> newItems) {}

    public void beforeUpdate(Map<Id, SObject> newItems, Map<Id, SObject> oldItems) {}

    public void beforeDelete(Map<Id, SObject> oldItems) {
        //
        Map<Id, Zuora__SubscriptionProductCharge__c> oldMap = (Map<Id, Zuora__SubscriptionProductCharge__c>) oldItems;

        System.debug('**Old code Block 1 begins - ' + Datetime.now());

        Set<Id> subID                        = new Set<Id>();
        Set<Id> accountID                    = new Set<Id>();
        Map<Id, String> acc_subscriptionsMap = new Map<Id, String>();

        for(Zuora__SubscriptionProductCharge__c subs : oldMap.values()) {

            if(subs.Zuora__Account__c != null) {
                acc_subscriptionsMap.put(subs.Zuora__Account__c, subs.Name);
                subID.add(subs.Id);
                accountID.add(subs.Zuora__Account__c);
            }
        } // End of for

        if(!acc_subscriptionsMap.isEmpty()) {
            SubscriptionProductChargeHelperClone.getallsubscriptions(
                accountID,
                true,
                subID
            );
        }

        System.debug('**Old code Block 1 ends - ' + Datetime.now());
        //
    }

    public void afterInsert(Map<Id, SObject> newItems) {
        //
        UpdatePrimaryOnAccountClone.getallSubscriptions(newItems.values());
        System.debug('@@Product Trigger ecxcuted');
        //
    }

    public void afterUpdate(Map<Id, SObject> newItems, Map<Id, SObject> oldItems) {
        //
        Map<Id, Zuora__SubscriptionProductCharge__c> newMap = (Map<Id, Zuora__SubscriptionProductCharge__c>) newItems;
        Map<Id, Zuora__SubscriptionProductCharge__c> oldMap = (Map<Id, Zuora__SubscriptionProductCharge__c>) oldItems;
        List<Account> parentAccount                         = new List<Account>();
        Set<Id> setAccountId                                = new Set<Id>();
        Map<Id, String> subscriptionMap                     = new Map<Id, String>();
        Map<Id, Account> accountMap                         = new Map<Id, Account>();
        Map<Id, String> acc_subscriptionsMap                = new Map<Id, String>();
        Set<Id> accountId                                   = new Set<Id>();
        List<Zuora__SubscriptionProductCharge__c> sub_list  = new List<Zuora__SubscriptionProductCharge__c>();
        List<Account> updateAccounts                        = new List<Account>();
        Map<Id, Account> updateAccMap                       = new Map<Id, Account>();

        SubscriptionProductChargeHelperClone.populateAccountMaps(
            setAccountId,
            accountMap,
            newMap
        );


        for(Zuora__SubscriptionProductCharge__c subsItr : newMap.values()) {

            if(subsItr.Zuora__Account__c != null) {

                if(subsItr.name.contains(ANNUAL)) {
                    Account acc = accountMap.get(subsItr.Zuora__Account__c);
                    acc.member_type__c = ANNUAL;
                    updateAccounts.add(acc);
                    updateAccMap.put(acc.Id, acc);
                } else if(subsItr.name.contains(MONTHLY)) {
                    Account acc1 = accountMap.get(subsItr.Zuora__Account__c);
                    acc1.member_type__c = MONTHLY;
                    updateAccounts.add(acc1);
                    updateAccMap.put(acc1.Id, acc1);
                } else if(subsItr.name.contains(DIAMOND)) {
                    Account acc2 = accountMap.get(subsItr.Zuora__Account__c);
                    acc2.member_type__c = DIAMOND;
                    updateAccounts.add(acc2);
                    updateAccMap.put(acc2.Id ,acc2);
                } else {
                    accountId.add(subsItr.Zuora__Account__c);
                }
            }
        } // End of for.

        if(!updateAccounts.isEmpty()) {
            update updateAccMap.values();
        }

        if(!acc_subscriptionsMap.isEmpty()) {
            SubscriptionProductChargeHelperClone.getallsubscriptions(accountId, false, null);
        }
        System.debug('**Old code Block 2 ends - ' + Datetime.now());

        System.debug('**New code Block 3 begins - ' + Datetime.now());
        UpdatePrimaryOnAccountClone.getallSubscriptionsforUpdate(newMap.values(), oldMap);
        System.debug('**New code Block 3 ends - ' + Datetime.now());
        //
    }

    public void afterDelete(Map<Id, SObject> oldItems) {
        Map<Id, Zuora__SubscriptionProductCharge__c> oldMap = (Map<Id, Zuora__SubscriptionProductCharge__c>) oldItems;
        UpdatePrimaryOnAccountClone.getallSubscriptions(oldMap.values());
    }

    public void afterUndelete(Map<Id, SObject> oldItems) {}
}