/*-----------------------------------------------------------------------------------------------------------------------------
*   Author:         Strategic Growth, Inc. (www.strategicgrowthinc.com)
*   Description:    This class is used to test the sgi_EventList_Controller controller class and EventList VF page
*
*   History:
*       Date            Developer       Comments
*       ----------      --------------  ---------------------------------------------------------------------------
*    	5/20/2016       CB              - Initial development
*       8/4/2016        CB              - Updates for Status, Sold Out, Timezone, Cancelled, Include in Calendar
*       10/13/2016      CB              - Add new custom field Event Title instead of Type
*    	12/14/2016    	JM        		- Add facility url filtering support
*    	12/19/2016    	JM        		- Add LEO filtering support
*		12/18/2017		CB				- Add Event Type (really Event Title) filtering
/*-----------------------------------------------------------------------------------------------------------------------------*/
public without sharing class sgi_EventList_Controller 
{
    private DateTime dtNow = DateTime.now();
    
    private string strSOQLSELECT = 'SELECT Id, Name, evt__City__c, evt__State__c, evt__Street__c, evt__Postal_Code__c, evt__Venue_Name__c, Is_Booked__c ' +
                                        ', evt__Event_Type__c, evt__Start__c, evt__End__c, evt__Event_Time_Zone__c, Event_URL__c, evt__Status__c ' +
                                        ', Event_Title__c ' +
                                    ' FROM evt__Special_Event__c ';
    
    private string strSOQLSELECT_AGGREGRATE = 'SELECT Event_Title__c ' +
        									' FROM evt__Special_Event__c ';
    
    private string strSOQLGROUPBY = ' GROUP BY Event_Title__c ';
    
    private string strSOQLWHERE = ' WHERE evt__Include_in_Calendar__c = true '+
                                    '   AND evt__Status__c IN (\'Published\', \'Cancelled\') ' +
                                    '   AND evt__Start__c >= :dtNow ';
    
    private string strSOQLWHERE_ST = ' AND evt__State__c IN (';
    private string strSOQLWHERENOT_ST = ' AND evt__State__c NOT IN (';
    
    private string strSOQLORDERBY = ' ORDER BY evt__Start__c ASC';
    private list<Event> pvtEvents;

    // Checks to see if the leo url parameter is set to true
    public boolean leo
    {
        get
        {
            string strLEO;
            strLEO = ApexPages.currentPage().getParameters().get('leo');
            if (strLEO == 'true') {
                return true;
            }   
            else {
                return false;
            }
        }
        set;
    }
    
    public string facID
    {
        get
        {
            string facID = 'Initialized';
            
            facID = ApexPages.currentPage().getParameters().get('facid');
            if (facID != null) {
                return facID;
            }   
            else {
                facID = 'none';
                return facID;
            }
        }
        set;
    }
    
    public string strState 
    {
        get
        {   
            string st = ApexPages.currentPage().getParameters().get('st');
            
            if (st != null && strState == null)
            {
                strState = st.trim().toUpperCase();
            }
            
            if (strState == null)
            {
                strState = 'None';
            }
            return strState;
        }
        
        set;
    }
    
    public list<SelectOption> lstStates
    {
        get
        {
            list<SelectOption> options = new List<SelectOption>();
            
            options.add(new SelectOption('None','Select Your State'));
            
            list<Event_List_State_Filters__c> lstStates = Event_List_State_Filters__c.getAll().values();
            lstStates.sort();
            
            for (Event_List_State_Filters__c e : lstStates)
            {
                if (e.Is_Active__c)
                {
                    options.add(new SelectOption(e.Name, e.Text_Displayed__c));
                }
            }
            
            return options;
        }
    }
    
    public string strEventType_URLParam
    {
        get
        {
            string evtType = ApexPages.currentPage().getParameters().get('evt_type');
            
            if (evtType != null)
            {
                strEventType_URLParam = evtType.escapeHtml4().trim();
            }
            else
            {
                strEventType_URLParam = 'None';
            }
            
            return strEventType_URLParam;
        }
        set;
    }
    
    public string strEventType 
    {
        get
        {   
            if (strEventType_URLParam != null && strEventType == null)
            {
                strEventType = strEventType_URLParam;
            }
            
            if (strEventType == null)
            {
                strEventType = 'None';
            }
            
            system.debug('*** sgi_EventList_Controller - strEventType: ' + strEventType);
            
            return strEventType;
        }
        
        set;
    }
    
    public list<SelectOption> lstEventTypes
    {
        get
        {
            list<SelectOption> options = new List<SelectOption>();
            
            options.add(new SelectOption('None','Select Event Type'));
            
            string strSOQL = strSOQLSELECT_AGGREGRATE + strSOQLWHERE;                         
            
            if (leo) 
            {
                strSOQL += ' AND LEO_Training__c = true';
            }
            else 
            {
                strSOQL += ' AND LEO_Training__c = false';
            }
            
            if (strState != '' && strState != 'None')
            {
                if (strState == 'USA' || strState == 'US')
                {
                    strSOQL += strSOQLWHERENOT_ST + '\'TX\') ';
                }
                else
                {
                    strSOQL += strSOQLWHERE_ST + '\'' + strState + '\') ';
                }
            }
            else 
            {
                if (facID != 'none')
                {
                    strSOQL += ' AND Facility__c = \'' + facID + '\'';
                }
            }
            
            strSOQL += strSOQLGROUPBY + ' ORDER BY Event_Title__c ';
            
            system.debug('*** sgi_EventList_Controller - lstEventTypes - strSOQL: ' + strSOQL);
            
            list<AggregateResult> lstAR = database.query(strSOQL);
            
            for (AggregateResult ar : lstAR)
            {
                options.add(new SelectOption((String)ar.get('Event_Title__c'), (String)ar.get('Event_Title__c')));
            }
            
            return options;
        }
    }
    
    public static Map<Integer, String> monthNamesMap = new Map<Integer, String> 
    {1 => 'January', 2 => 'February', 3 => 'March', 4 => 'April', 5 => 'May', 6 => 'June',
        7 => 'July', 8 => 'August', 9 => 'September', 10 => 'October', 11 => 'November', 12 => 'December' };
            
            public List<String> lstMonths 
        {
            get 
            {
                // create list with current month + next 2 months
                lstMonths = new List<String>();
                Date todayDt = Date.today();
                
                lstMonths.add(monthNamesMap.get(todayDt.month()));
                lstMonths.add(monthNamesMap.get(todayDt.addMonths(1).month()));
                lstMonths.add(monthNamesMap.get(todayDt.addMonths(2).month()));
                
                return lstMonths;
            }
            
            set;
        }
    
    public Map<String, List<Event>> mapEvents 
    {
        get 
        {
            mapEvents = new Map<String, List<Event>>();
            
            // initialize map with empty lists in case a month doesn't have events
            for(String m : lstMonths) 
            {
                List<Event> evtLst = new List<Event>();
                mapEvents.put(m, evtLst);
            }
            
            for(Event e : lstEvents) 
            {
                Integer eventMonth = e.objEventRecord.evt__Start__c.month();
                String monthName = monthNamesMap.get(eventMonth);
                if(monthName == null) 
                {
                    continue;
                }
                
                List<Event> evtLst = mapEvents.get(monthName);
                if(evtLst == null) 
                {
                    continue;
                }
                evtLst.add(e);
                
                mapEvents.put(monthName, evtLst);
            }
            
            return mapEvents;
        }
        set;
    }
    
    public list<Event> lstEvents
    {
        get
        {
            if (pvtEvents == null)
            {       
                pvtEvents = new list<Event>();
                
                string strSOQL = strSOQLSELECT + strSOQLWHERE;                         

                if (leo) {
                    strSOQL += ' AND LEO_Training__c = true ';
                }
                else {
                    strSOQL += ' AND LEO_Training__c = false ';
                }
                
                if (strEventType != '' && strEventType != 'None')
                {
                    strSOQL += ' AND Event_Title__c = :strEventType ';
                }
                
                if (strState != '' && strState != 'None')
                {
                    if (strState == 'USA' || strState == 'US')
                    {
                        strSOQL += strSOQLWHERENOT_ST + '\'TX\') ';
                    }
                    else
                    {
                        strSOQL += strSOQLWHERE_ST + '\'' + strState + '\') ';
                    }
                }
                else {
                    if (facID != 'none'){
                        strSOQL += ' AND Facility__c = \'' + facID + '\' ';
                    }
                }

                strSOQL += strSOQLORDERBY;
                
                system.debug('*** sgi_EventList_Controller - lstEvents - strSOQL: ' + strSOQL);
                
                Event clsEvent;
                
                for (evt__Special_Event__c e : database.query(strSOQL))
                {
                    clsEvent = new Event(e);
                    pvtEvents.add(clsEvent);
                }
            }
            
            return pvtEvents;
        }
    }
    
    public sgi_EventList_Controller() 
    {
        
    }
    
    public pagereference LoadEvents()
    {
        pvtEvents = null;
        pagereference pr = Page.EventList;
        
        string etParam = (strEventType == 'None' || strEventType == null) ? strEventType_URLParam : strEventType;
        
        pr.getParameters().put('evt_type', etParam.unescapeHtml4());
        pr.getParameters().put('st', strState);
        
        pr.setRedirect(true);

        return pr;
    }
    
    public class Event
    {
        public evt__Special_Event__c objEventRecord {get; set;}
        
        public Integer intDay 
        {
            get 
            {
                if (intDay == null && objEventRecord.evt__Start__c != null ) 
                {
                    intDay = objEventRecord.evt__Start__c.day();
                }
                
                return intDay;
            }
            set;
        }
        
        public string strCity
        {
            get
            {
                string retVal = '';
                
                if (objEventRecord != null && objEventRecord.evt__City__c != null)
                {
                    retVal = objEventRecord.evt__City__c;
                }
                
                return retVal;
            }
        }
        
        public string strStatePostalCode
        {
            get
            {
                string retVal = '';
                
                if (objEventRecord != null)
                {
                    if (objEventRecord.evt__State__c != null)
                    {
                        retVal = objEventRecord.evt__State__c;
                    }
                    if (objEventRecord.evt__Postal_Code__c != null) 
                    {
                        retVal += ' ' + objEventRecord.evt__Postal_Code__c;
                    }
                }
                
                return retVal;
            }
        }
        
        public string strTimeZone
        {
            get
            {
                string retVal = '';
                string strTimeZone = 'America/Chicago';
                
                if (objEventRecord != null && objEventRecord.evt__Event_Time_Zone__c != null)
                {
                    strTimeZone = objEventRecord.evt__Event_Time_Zone__c;
                    retVal = strTimeZone.substring(strTimeZone.lastIndexOf('(') + 1).replace(')', '');
                }
                
                return retVal;
            }
        }
        
        public string strDate
        {
            get
            {
                string retVal = '';
                
                if (objEventRecord != null && objEventRecord.evt__Start__c != null)
                {
                    DateTime dtStart = DateTime.newInstance(objEventRecord.evt__Start__c.dateGMT(), objEventRecord.evt__Start__c.timeGmt());
                    retVal = objEventRecord.evt__Start__c.format('MM/dd/yyyy', strTimeZone);
                }
                
                return retVal;
            }
        }
        
        public string strTime
        {
            get
            {
                string retVal = '';
                
                if (objEventRecord != null)
                { 
                    if (objEventRecord.evt__Start__c != null)
                    {
                        //DateTime dtStart = DateTime.newInstance(objEventRecord.evt__Start__c.dateGMT(), objEventRecord.evt__Start__c.timeGmt());
                        retVal = objEventRecord.evt__Start__c.format('h:mm a', strTimeZone);
                    }
                    
                    if (objEventRecord.evt__End__c != null)
                    {
                        //DateTime dtEnd = DateTime.newInstance(objEventRecord.evt__End__c.dateGMT(), objEventRecord.evt__End__c.timeGmt());
                        //retVal += ' to ' + objEventRecord.evt__End__c.format('h:mm a zzz', strTimeZone);
                        retVal += ' to ' + objEventRecord.evt__End__c.format('h:mm a', strTimeZone);
                    }       
                }
                
                return retVal;
            }
        }
        
        public string strLocation
        {
            get
            {
                string retVal = '';
                
                if (objEventRecord != null && objEventRecord.evt__Venue_Name__c != null)
                {
                    retVal = objEventRecord.evt__Venue_Name__c;
                }
                
                return retVal;
            }
        }
        
        public string EventType
        {
            get
            {
                string retVal = '';
                
                if (objEventRecord != null && objEventRecord.Event_Title__c != null)
                {
                    retVal = objEventRecord.Event_Title__c;
                }
                
                return retVal;
            }
        }
        
        public string RegisterURL
        {
            get
            {
                string retVal = '#';
                
                if (objEventRecord != null && objEventRecord.Event_URL__c != null)
                {
                    retVal = objEventRecord.Event_URL__c;
                }
                
                return retVal;
            }
        }
        
        public string EventStatus
        {
            get
            {
                string retVal = '';
                
                if (objEventRecord != null && objEventRecord.evt__Status__c  != null)
                {
                    retVal = objEventRecord.evt__Status__c ;
                }
                
                return retVal;
            }
        }
        
        public boolean isCancelled
        {
            get
            {
                boolean retVal = false;
                
                if (objEventRecord != null)
                {
                    retVal = objEventRecord.evt__Status__c == 'Cancelled'? true : false;
                }
                
                return retVal;
            }
        }
        
        public boolean isBooked
        {
            get
            {
                boolean retVal = false;
                
                if (objEventRecord != null)
                {
                    retVal = objEventRecord.Is_Booked__c;
                }
                
                return retVal;
            }
        }
        
        public Event (evt__Special_Event__c objEvent)
        {
            objEventRecord = objEvent;
        }
    }
}