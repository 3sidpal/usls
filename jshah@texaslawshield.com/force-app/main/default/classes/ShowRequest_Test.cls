@isTest
private class ShowRequest_Test 
{

	private static testMethod void test() 
	{
        Account objAcc = new Account();
        objAcc.Name = 'test acc';
        objAcc.RecordTypeId = Schema.SObjectType.Account.RecordTypeInfosByName.get('Facility (Prospect)').RecordTypeId;
        insert objAcc;
        Account objAcc1 = new Account();
        objAcc1.Name = 'test acc';
        objAcc1.RecordTypeId = Schema.SObjectType.Account.RecordTypeInfosByName.get('Promoter Account').RecordTypeId;
        insert objAcc1;
        contact objContact = new Contact(LastName = 'test con1', AccountId = objAcc.id);
        insert objContact;
        
        Case objCase = new Case();
        objCase.Host_Facility__c = objAcc.Id;
        ApexPages.currentPage().getParameters().put('reqName', 'test name');
        ApexPages.currentPage().getParameters().put('reqEmail', 'test@test.com');
        ApexPages.currentPage().getParameters().put('promoterId', objAcc1.id);
        ApexPages.currentPage().getParameters().put('promoterContact', '');
        ApexPages.currentPage().getParameters().put('promoterPhone', '88763553');
        ApexPages.currentPage().getParameters().put('showName', 'Name');
        ApexPages.currentPage().getParameters().put('showType', 'Type');
        ApexPages.currentPage().getParameters().put('state', 'NY');
        ShowRequest objShowRequest = new ShowRequest(new ApexPages.StandardController(objCase));
        objShowRequest.strFacilityContact = '';
        list<SelectOption> lstFacilityContacts = objShowRequest.lstFacilityContacts;
        String strPhone = objShowRequest.fetchPromoterPhone;
        list<SelectOption> lstEventTypes = objShowRequest.lstEventTypes;
        String strReqEmail = objShowRequest.strReqEmail;
        String strReqName = objShowRequest.strReqName;
        String strCaseType = objShowRequest.strCaseType;
        String strCaseNumber = objShowRequest.strCaseNumber;
        objShowRequest.strFacilityContact = objContact.id;
        objShowRequest.addMore();
        objShowRequest.SaveRequest();
        objShowRequest.cloneData();
        strCaseNumber = objShowRequest.strCaseNumber;
	}
}