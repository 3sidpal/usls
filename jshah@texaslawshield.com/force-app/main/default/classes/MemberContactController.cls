public class MemberContactController 
{
 public Integer dndLen {get; set;}
 public Integer numPages {get; set;}
 public Integer pageQuery {get; set;}
 public String urlQuery {get; set;}
 public String accId {get; set;}
 public List<Account> accounts {get; set;}
 
  public MemberContactController ()
  {
    urlQuery = ApexPages.currentPage().getParameters().get('query');
   
    if ( ( null != urlQuery ) && ( 0 != urlQuery.length() ) ) {
       pageQuery = Integer.valueOf(urlQuery);
       executeSearch();
    }
  }
  
  public PageReference BuildMemberContactList ()
  {
    deleteMemberContacts();
    addMemberContacts();
            
    return null;
  }
 
  public PageReference ApplyDoNotDisturb ()
  {
    Date todayDate = Date.today();
    Account dndAccount = new Account(id=accId);
    
    if ( ( null != dndAccount ) ) {
      dndAccount.DND_End__c = todayDate.addDays(dndLen);

      update dndAccount;
      deleteMemberContact(accId);
    }
    
    executeSearch();
    return null;
  }
  
  public PageReference executeSearch ()
  {
    urlQuery = ApexPages.currentPage().getParameters().get('query');
    accounts = [SELECT Account.id, Account.fb_connect__Account_Name__c, Account.Name, Account.Phone,
        Account.Subscription_Value__c, Account.fb_connect__FB_Status__c, Account.fb_connect__FB_StatusTimestamp__c,
        Account.fb_connect__FB_Customer_ID__c, Account.fb_connect__FB_Customer_Number__c, Account.Member_Contact_Record_Number__c, Account.DND_End__c
        FROM Account WHERE Account.id IN(SELECT Account__c FROM MemberContact__c WHERE Page__c = :pageQuery)];
            
    return new PageReference('/apex/MemberContact?query=' + urlQuery);
  }
 
  private void deleteMemberContact ( string accountId )
  {
    List<MemberContact__c> contacts = [SELECT Id FROM MemberContact__c WHERE Account__c = :accountId];
    
    delete contacts;
  }
 
  private void deleteMemberContacts ()
  {
    List<MemberContact__c> contacts = [SELECT Id FROM MemberContact__c];
    
    delete contacts;
  }
  
  private void addMemberContacts ()
  {
    integer currPage = 1;
    integer currPageRecord = 1;
    integer currRecord = 2;
    integer total = [SELECT COUNT() FROM Account WHERE fb_connect__FB_Status__c = 'ApprovedCreditCardWarning' AND (DND_End__c <= TODAY OR DND_End__c = NULL) LIMIT 1000];
    double perPage = Math.ceil(total / numPages);
    List<MemberContact__c> memberContacts = new List<MemberContact__c>();
    List<Account> ccwAccounts = new List<Account>();
    
    for (Account a :[SELECT id FROM Account WHERE fb_connect__FB_Status__c = 'ApprovedCreditCardWarning' AND (DND_End__c <= TODAY OR DND_End__c = NULL) LIMIT 1000]) {
      Account ccwAccount = new Account(id=a.id);
      MemberContact__c contact = new MemberContact__c();
      
      ccwAccount.Member_Contact_Record_Number__c = currRecord;
      contact.Account__c = a.id;
      contact.Record_Number__c = currRecord;
      
      if ( currPageRecord > perPage ) {
        currPage++;
        currPageRecord = 1;
      }
      
      contact.Page__c = currPage;
      
      ccwAccounts.add(ccwAccount);
      memberContacts.add(contact);
      currPageRecord++;
      currRecord++;
    }
    
    Database.update(ccwAccounts, false);
    Database.insert(memberContacts, false);
  }

}