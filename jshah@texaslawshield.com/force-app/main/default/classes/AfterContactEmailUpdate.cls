/**  
* ─────────────────────────────────────────────────────────────────────────────────────────────────┐
* @author         Unmesha Punyamurthula
* @version        1.0
* @created        06/04/2019
* @TestClass      AfterContactEmailUpdateTest
* @Trigger        ContactTrigger
* @Description    This Class is used to find the duplicate Contact Email's 
* ─────────────────────────────────────────────────────────────────────────────────────────────────┘
*/
public class AfterContactEmailUpdate{
    
    public static void beforeUpDateMethod(List<Contact> newConList, Map<id, Contact> oldConMap)
        
    {
        
        List<Contact> contList = new List<Contact>();// New Contact List 
        Map<string, id> conMap = new Map<string, id>(); // Contact Map with email as key and id as value
        Map<String, boolean> AccMap = new Map<string, boolean>(); // Account Map with Email as Key and bollean as Value
        Set<String> AllEmails = new Set<String>(); // email set to store all the unique emails 
       
        // Iterate throug new Contacts and add the email to conMap
        for(Contact con: newConList){
            if(con.email != null){
                conMap.put(con.email,con.id);// if email is not null add to the ConMap
            }else{
                con.Duplicate_Contact__c = false; // by default make Duplicate_Contact__c as false
            }
        }
        
       // system.debug('@@conMpa'+ConMap);
       // system.debug('@@conMpa'+ConMap.keySet());
        
        // Query all Accounts where email id in conMap.KeySet    
        List<Account> AccSearch = [Select id, PersonEmail,SecondaryEmailAddress__c from Account where PersonEmail in:conMap.keyset()];
        
        //List<Account> AccSearch2 = [Select id, PersonEmail,SecondaryEmailAddress__c,Secondary_Email__c from Account where 
       									//SecondaryEmailAddress__c in:conMap.keyset() AND SecondaryEmailAddress__c!=null limit 1 ];
        
                                              
        //system.debug('@@'+AccSearch2);
        if(AccSearch.size()>0){
            for(Account acc: AccSearch){ // iterate through the accounts list
                if(acc.PersonEmail != null){
                   accMap.put(acc.PersonEmail, true);  // if Person email not equals to null add it to accMap 
                }
            }
        }
        
      /*  if(AccSearch2.size()>0){
            for(Account acc1: AccSearch2){
                If(acc1.Secondary_Email__c !=null){
                   accMap.put(acc1.Secondary_Email__c, true); 
                }  
            }
        }
*/
        
        
       // system.debug('@@27'+accMap);
        for(Contact con: newConList){ // iterate tthrough the newly created Contacts 
            if(con.email != null){
                if(accMap.containsKey(con.email)){ // if contatcs email exist in accMap then make Duplicate contact as Ture else false
                    con.Duplicate_Contact__c = true;
                }else{
                    con.Duplicate_Contact__c = false;
                }
            }else{
                con.Duplicate_Contact__c = false; 
            }
        }
   }
}