/*-----------------------------------------------------------------------------------------------------------------------------
*   Author:         Strategic Growth, Inc. (www.strategicgrowthinc.com)
*   Description:    This class is used to test the sgi_EventList_Controller controller class and EventList VF page
*
*   History:
*       Date            Developer       Comments
*       ----------      --------------  ---------------------------------------------------------------------------
*       5/20/2016       CB              - Initial development
*    	8/4/2016    	CB        		- Update the tests for the changes to the class for (Sold Out, Timezone, Cancelled and other updates)
*		12/18/2017		CB				- Update to test for the new Event Type (really title) filtering
/*-----------------------------------------------------------------------------------------------------------------------------*/

@isTest
private class sgi_EventList_Controller_Test 
{
    // Method used to create a new Event
    private static evt__Special_Event__c createNewEvent(evt__Special_Event__c objE, String strName, String strStatus, String strState, Boolean blnInclude, String testFacID, boolean isLEO)
    {
        if (objE == null)
        {
            objE = new evt__Special_Event__c();
        }
        
        DateTime dtStart = DateTime.now().addDays(1);
        
        objE.Name = strName;
        objE.evt__City__c = 'Houston';
        objE.evt__State__c = strState;
        objE.evt__Street__c = '111 Main St.';
        objE.evt__Postal_Code__c = '77002';
        objE.evt__Venue_Name__c = 'The Venue';
        objE.evt__Event_Type__c = 'Gun Law Seminar';
        objE.Event_Title__c = 'Test Title-' + strState;
        objE.evt__Start__c = dtStart;
        objE.evt__End__c = dtStart.addHours(3);
        objE.evt__Event_Time_Zone__c = '(GMT-05:00) Central Daylight Time (America/Chicago)';
        objE.evt__Status__c = strStatus;
        objE.evt__Include_in_Calendar__c = blnInclude;
        objE.RecordTypeId = Schema.SObjectType.evt__Special_Event__c.getRecordTypeInfosByName().get('Quick Event').getRecordTypeId();
        objE.Facility__c = testFacID;
        objE.LEO_Training__c = isLEO;
        
        insert objE;
        
        return objE;
    }
    
    private static void LoadStateFilters()
    {
        list<Event_List_State_Filters__c> lstStates = Event_List_State_Filters__c.getAll().values();
        Event_List_State_Filters__c objState;
        
        if (lstStates == null || lstStates.isEmpty())
        {
            lstStates.add(new Event_List_State_Filters__c(Name='TX', Text_Displayed__c='Texas', Is_Active__c=true));
            lstStates.add(new Event_List_State_Filters__c(Name='OK', Text_Displayed__c='Oklahoma', Is_Active__c=true));
        }
    }
    
    static testMethod void test1() 
    {
        LoadStateFilters();
        
        list<evt__Special_Event__c> lstEvents = new list<evt__Special_Event__c>();
        
        // Create a few Events
        evt__Special_Event__c objE1 = createNewEvent(new evt__Special_Event__c(), 'Testing 1', 'Published', 'TX', true, '001i000001Quxcw', false);
        evt__Special_Event__c objE2 = createNewEvent(new evt__Special_Event__c(), 'Testing 2', 'Published', 'TX', true, '001i000001Quxcw', false);
        evt__Special_Event__c objE3 = createNewEvent(new evt__Special_Event__c(), 'Testing 3', 'Cancelled', 'OK', true, '001i000001Quxcw', false);
        evt__Special_Event__c objE4 = createNewEvent(new evt__Special_Event__c(), 'Testing 4', 'Cancelled', 'TX', false, '001i000001Quxcw', false);
        
        // This event SHOULD NOT show on the screen
        evt__Special_Event__c objE5 = createNewEvent(new evt__Special_Event__c(), 'Testing 5', 'Planned', 'TX', true, '001i000001Quxcw', false);
        
        // Create a reference to the custom controller
        sgi_EventList_Controller cntrl = new sgi_EventList_Controller();
        
        // Create an instance of the page
        PageReference pr = Page.EventList;
        
        Test.setCurrentPage(pr);
        
        // Check how many records are in the list
        system.assertEquals(3, cntrl.lstEvents.size());
        
        // Call the public properties (get methods)
        string strS = cntrl.strState;
        list<SelectOption> lstStates = cntrl.lstStates;
        sgi_EventList_Controller.Event clsEvent = new sgi_EventList_Controller.Event(objE1);
        evt__Special_Event__c evt = clsEvent.objEventRecord;
        string sCity = clsEvent.strCity;
        string sSPC = clsEvent.strStatePostalCode;
        string sD = clsEvent.strDate;
        string sT = clsEvent.strTime;
        string sL = clsEvent.strLocation;
        string sType = clsEvent.EventType;
        string sURL = clsEvent.RegisterURL;
        integer iDay = clsEvent.intDay;
        boolean bCancelled = clsEvent.isCancelled;
        string sStatus = clsEvent.EventStatus;
        boolean bBooked = clsEvent.isBooked;
        
        // Call the public properties (set methods)
        clsEvent.objEventRecord = objE1;
        cntrl.strState = 'TX';
        
        // Reload the screen
        cntrl.LoadEvents();
        
        // Check how many records are in the list
        system.assertEquals(2, cntrl.lstEvents.size());
        
        // increase test coverage
        List<String> lstMonths = cntrl.lstMonths;
        Map<String, List<sgi_EventList_Controller.Event>> mapEvents = cntrl.mapEvents;
    }
    
    static testMethod void testParameter() 
    {
        LoadStateFilters();
        
        list<evt__Special_Event__c> lstEvents = new list<evt__Special_Event__c>();
        
        // Create a few Events
        evt__Special_Event__c objE1 = createNewEvent(new evt__Special_Event__c(), 'Testing 1', 'Published', 'TX', true, '001i000001Quxcw', false);
        evt__Special_Event__c objE2 = createNewEvent(new evt__Special_Event__c(), 'Testing 2', 'Published', 'TX', true, '001i000001Quxcw', false);
        evt__Special_Event__c objE3 = createNewEvent(new evt__Special_Event__c(), 'Testing 3', 'Cancelled', 'OK', true, '001i000001Quxcw', false);
        evt__Special_Event__c objE4 = createNewEvent(new evt__Special_Event__c(), 'Testing 4', 'Cancelled', 'TX', false, '001i000001Quxcw', false);
        
        // This event SHOULD NOT show on the screen
        evt__Special_Event__c objE5 = createNewEvent(new evt__Special_Event__c(), 'Testing 5', 'Planned', 'TX', true, '001i000001Quxcw', false);
        
        // Create a reference to the custom controller
        sgi_EventList_Controller cntrl = new sgi_EventList_Controller();
        
        // Create an instance of the page
        PageReference pr = Page.EventList;
        
        // Pass in the parameter
        
        pr.getParameters().put('st', 'TX');
        
        Test.setCurrentPage(pr);
        
        // Check how many records are in the list
        system.assertEquals(2, cntrl.lstEvents.size());
        
        // Change the strState value to simulate a user setting the filter drop down
        cntrl.strState = 'OK';
        
        // Reload the screen
        cntrl.LoadEvents();
        
        // Check how many records are in the list
        system.assertEquals(1, cntrl.lstEvents.size());
    }
    
    static testMethod void testEventTypeFilter() 
    {
        LoadStateFilters();
        
        list<evt__Special_Event__c> lstEvents = new list<evt__Special_Event__c>();
        
        // Create a few Events
        evt__Special_Event__c objE1 = createNewEvent(new evt__Special_Event__c(), 'Testing 1', 'Published', 'TX', true, '001i000001Quxcw', false);
        evt__Special_Event__c objE2 = createNewEvent(new evt__Special_Event__c(), 'Testing 2', 'Published', 'TX', true, '001i000001Quxcw', false);
        evt__Special_Event__c objE3 = createNewEvent(new evt__Special_Event__c(), 'Testing 3', 'Cancelled', 'OK', true, '001i000001Quxcw', false);
        evt__Special_Event__c objE4 = createNewEvent(new evt__Special_Event__c(), 'Testing 4', 'Cancelled', 'TX', false, '001i000001Quxcw', false);
        
        // This event SHOULD NOT show on the screen
        evt__Special_Event__c objE5 = createNewEvent(new evt__Special_Event__c(), 'Testing 5', 'Planned', 'TX', true, '001i000001Quxcw', false);
        
        // Create a reference to the custom controller
        sgi_EventList_Controller cntrl = new sgi_EventList_Controller();
        
        // Create an instance of the page
        PageReference pr = Page.EventList;
        
        // Pass in the parameter
        pr.getParameters().put('evt_type', 'Test Title-TX');
        
        Test.setCurrentPage(pr);
        
        // Check how many records are in the list
        system.assertEquals(2, cntrl.lstEvents.size());
        
        // Change the strEventType value to simulate a user setting the filter drop down
        cntrl.strEventType = 'Test Title-OK';
        
        // Reload the screen
        cntrl.LoadEvents();
        
        // Check how many records are in the list
        system.assertEquals(1, cntrl.lstEvents.size());
        
        // Call the new public properties for Event Filtering
        string strET = cntrl.strEventType;
        list<SelectOption> lstSOET = cntrl.lstEventTypes;
    }
    
    static testMethod void testFacilityID() 
    {     
        LoadStateFilters();
        
        list<evt__Special_Event__c> lstEvents = new list<evt__Special_Event__c>();
        
        // Create a few Events
        evt__Special_Event__c objE1 = createNewEvent(new evt__Special_Event__c(), 'Testing 1', 'Published', 'TX', true, '001i000001Quxcw', false);
        evt__Special_Event__c objE2 = createNewEvent(new evt__Special_Event__c(), 'Testing 2', 'Published', 'TX', true, '001i000001Quxcw', false);
        evt__Special_Event__c objE3 = createNewEvent(new evt__Special_Event__c(), 'Testing 3', 'Cancelled', 'OK', true, '001i000001Quxcw', false);
        evt__Special_Event__c objE4 = createNewEvent(new evt__Special_Event__c(), 'Testing 4', 'Cancelled', 'TX', true, '001i000001Quxcw', false);
        
        // This event SHOULD NOT show on the screen
        evt__Special_Event__c objE5 = createNewEvent(new evt__Special_Event__c(), 'Testing 5', 'Planned', 'TX', true, '001i000001Quxcw', false);
        
        // Create a reference to the custom controller
        sgi_EventList_Controller cntrl = new sgi_EventList_Controller();
        
        // Create an instance of the page
        PageReference pr = Page.EventList;
        
        // Pass in the parameter
        pr.getParameters().put('facid', '001i000001Quxcw');
        
        Test.setCurrentPage(pr);
        
        // Check how many records are in the list
        system.assertEquals(4, cntrl.lstEvents.size());
        
        // Change the strState value to simulate a user setting the filter drop down
        cntrl.strState = 'OK';
        
        // Reload the screen
        cntrl.LoadEvents();
        
        // Check how many records are in the list
        //system.assertEquals(4, cntrl.lstEvents.size());
    }
    
    static testMethod void testLEO() 
    {    
        LoadStateFilters();
        
        list<evt__Special_Event__c> lstEvents = new list<evt__Special_Event__c>();
        
        // Create a few Events
        evt__Special_Event__c objE1 = createNewEvent(new evt__Special_Event__c(), 'Testing 1', 'Published', 'TX', true, '001i000001Quxcw', false);
        evt__Special_Event__c objE2 = createNewEvent(new evt__Special_Event__c(), 'Testing 2', 'Published', 'TX', true, '001i000001Quxcw', true);
        evt__Special_Event__c objE3 = createNewEvent(new evt__Special_Event__c(), 'Testing 3', 'Cancelled', 'OK', true, '001i000001Quxcw', false);
        evt__Special_Event__c objE4 = createNewEvent(new evt__Special_Event__c(), 'Testing 4', 'Cancelled', 'TX', true, '001i000001Quxcw', true);
        
        // This event SHOULD NOT show on the screen
        evt__Special_Event__c objE5 = createNewEvent(new evt__Special_Event__c(), 'Testing 5', 'Planned', 'TX', true, '001i000001Quxcw', false);
        
        // Create a reference to the custom controller
        sgi_EventList_Controller cntrl = new sgi_EventList_Controller();
        
        // Create an instance of the page
        PageReference pr = Page.EventList;
        
        // Pass in the parameter
        pr.getParameters().put('leo', 'true');
        
        Test.setCurrentPage(pr);
        
        // Check how many records are in the list
        system.assertEquals(2, cntrl.lstEvents.size(), 'LEO AssertEquals Failed!');
        
        // Reload the screen
        //cntrl.LoadEvents();
        
        // Check how many records are in the list
        //system.assertEquals(4, cntrl.lstEvents.size());
    }    
}