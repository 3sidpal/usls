@isTest                    
public class MemberContactControllerTests {

    public static testMethod void testMemberContactController() 
    {
        PageReference pageRef = Page.MemberContact;
        Test.setCurrentPage(pageRef);
        
        ApexPages.currentPage().getParameters().put('query', '10');
        
        MemberContactController controller = new MemberContactController();
        
        String nextPage = controller.executeSearch().getUrl();

        System.assertEquals('/apex/MemberContact?query=10', nextPage);
    }
}