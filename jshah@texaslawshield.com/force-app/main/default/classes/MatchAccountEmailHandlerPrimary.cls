/**  
* ─────────────────────────────────────────────────────────────────────────────────────────────────┐
* @author         Unmesha Punyamurthula
* @version        1.0
* @created        06/04/2019
* @TestClass      MatchAccountEmailHandlerTest
* @Trigger        AccountTriggerAfter
* @Description    This Class is used to find the duplicate Contact Email's 
* ─────────────────────────────────────────────────────────────────────────────────────────────────┘
*/

public class MatchAccountEmailHandlerPrimary {
    
    public static void findDuplicateEmail(List<Account> newAccList,Map<id,Account> oldMap){
        if(newAccList != null && newAccList.size() > 0){
            Set<String> emailSet = new Set<string>();
            Set<String> oldEmailSet = new Set<string>();
             Set<String> AllEmailSet = new Set<string>();
            for(Account acc: newAccList){
                if(Trigger.isinsert){
                    if(acc.SecondaryEmailAddress__c != null){
                        emailSet.add(acc.SecondaryEmailAddress__c);
                    }
                }else if(Trigger.isUpdate){
                    Account oldacc = oldMap.get(acc.Id);
                    if(acc.SecondaryEmailAddress__c != oldacc.SecondaryEmailAddress__c){
                        if(acc.SecondaryEmailAddress__c != null){
                        	emailSet.add(acc.SecondaryEmailAddress__c);
                        }
                        if(oldacc.SecondaryEmailAddress__c != null){
                            oldEmailSet.add(oldacc.SecondaryEmailAddress__c);
                        }
                    }
                }
                else if(Trigger.isDelete){
                    if(acc.SecondaryEmailAddress__c != null){
                        oldEmailSet.add(acc.SecondaryEmailAddress__c);
                    }
                }
            }
            if(emailSet.size() > 0){
            	AllEmailSet.addAll(emailSet);
            }
            if(oldEmailSet.size() > 0){
            	AllEmailSet.addAll(oldEmailSet);
            }
            //system.debug('emailSet'+emailSet);
            //system.debug('oldEmailSet'+oldEmailSet);
            if(AllEmailSet.size() > 0){
                List<Contact> conList = [Select Id ,Secondary_Duplicate_Contact__c,  Email FROM COntact WHERE email IN : AllEmailSet];
                if(conLIst.size() > 0){
                     for(Contact con: conList){
                         //system.debug('con');
                        if(emailSet.contains(con.Email)){
                            con.Secondary_Duplicate_Contact__c = true;
                            //system.debug('true');
                        }else if(oldEmailSet.contains(con.Email)){
                            con.Secondary_Duplicate_Contact__c = false;
                            //system.debug('false');
                        }
                         //system.debug('end');
                    }
                    update conList;
                }
            }
        }
    }
}