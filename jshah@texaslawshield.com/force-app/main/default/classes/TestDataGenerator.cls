public class TestDataGenerator {
 
      //Always allow for the creation of one or more users
      public List<User> users;
      public Map<String,List<User>> profileToUserMap;      
 
      /*private List<Clone_Settings__c> cloneSettings;
      private MarketingProjectPageSetting__c mpSettings;   // Marketing Project Settings
      private MarketingProjectChatterSettings__c mpChatSettings; // MP Chatter Settings
      */ 
      
      public List<Account> accounts;
      public List<Contact> contacts;
      public List<Opportunity> opportunities;
      public List<Campaign> campaigns;
      //public List<Task> campaignTasks;
      public List<Task> Tasks;
      public List<Event> Events;
      //public List<Task> mpTasks;  
      //public List<Cost__c> costs;    
      public List<Lead> leads;
      
      /*public List<Booth__c> booths;
      public List<Booth_Assignment__c> boothAssignments;
        */
        
      /*
      public Pricebook2 pricebook; 
      public List<Product2> products;
      public List<PricebookEntry> pricebookEntries;
      public List<OpportunityLineItem> opportunityLineItems;
      */
      
      
      
      public Integer userNum;
      public Integer accountNum;
      public Integer contactNum;
      public Integer campaignNum;
      //public Integer marketingProjectNum;
      //public Integer campaignTaskNum;
      //public Integer mpTaskNum;
      public Integer TaskNum;
      public Integer EventNum;
      //public Integer costNum;
      
      public Integer opptyNum;
      public Integer opptyLineItemNum;
      public Integer productNum;
      public Integer pricebookEntryNum;      
      public Integer leadNum;
      public Integer boothNum;
      
      
      public TestDataGenerator(){
            userNum = 5;
            accountNum = 5;
            contactNum = 5;
            
            // Create Campaigns
            campaignNum = 2;
            //marketingProjectNum = campaignNum;
            //campaignTaskNum = campaignNum;
            //mpTaskNum  = campaignNum;
            TaskNum = 5;
            EventNum = 5;    
            
            opptyNum = 5;
            opptyLineItemNum = opptyNum * 20;
            productNum = 10;
            pricebookEntryNum = productNum;
            
            //costNum = campaignNum;
            
            leadNum = 5;
            //boothNum = 2;
      }
      
/*
            public List<Cost__c> getCosts(Boolean xCommit) {
            
                if(costs == null) {
                      costs = new List<Cost__c>();
                      for(Integer i = 0; i < costNum; i++) {
                            Cost__c c = new Cost__c();
                            c.Name          = 'Cost Record ' + i;
                            c.Actual_Cost__c = 10;
                            c.Est_Cost__c   = 5;
                            c.Campaign__c      = getCampaigns(true)[i].Id;
                            c.Type__c = 'Fee';
                            costs.add(c);
                      }
                      if(xCommit) insert costs;
                }
                
                if(xCommit && costs.size() > 0 && costs[0].Id == null) insert costs;                                            
                
                return costs;
          }      
      */
        /*  
    public List<Marketing_Project__c> getmarketingProjects(Boolean xCommit) {
            
                Date myDate = date.newinstance(2013, 1, 15);
            
                if(marketingProjects == null) {
                      marketingProjects = new List<Marketing_Project__c>();
                      for(Integer i = 0; i < marketingProjectNum; i++) {
                            Marketing_Project__c mp = new Marketing_Project__c();
                            mp.Name             = 'Test MP Records ' + i;
                            mp.Project_Type__c  = 'TEST';
                            mp.Campaign__c      = getCampaigns(true)[i].Id;
    
                            marketingProjects.add(mp);
                      }
                      if(xCommit) insert marketingProjects;
                }
                
                if(xCommit && marketingProjects.size() > 0 && marketingProjects[0].Id == null) insert marketingProjects;                                            
                
                return marketingProjects;
          }
*/

        public List<Task> getTasks(Boolean xCommit) {
            
                Date myDate = date.newinstance(2013, 1, 15);
            
                if(Tasks == null) {
                      Tasks = new List<Task>();
                      for(Integer i = 0; i < TaskNum; i++) {
                            Task t = new Task();
                            t.subject               = 'Test Subject ' + i;
                            t.type                  = 'Email';
                            t.ActivityDate          = myDate;
                            t.status                = 'In Progress';
                            t.priority              = 'Normal';
                            
                            Integer j = Math.mod(i,getAccounts(true).size());
                            t.WhatId                = getAccounts(true)[0].Id;
           
                            Tasks.add(t);
                      }
                      if(xCommit) insert Tasks;
                }
                
                if(xCommit && Tasks.size() > 0 && Tasks[0].Id == null) insert Tasks;                                            
                
                return Tasks;
          }


         public List<Event> getEvents(Boolean xCommit) {
            
                Date myDate = date.newinstance(2019, 1, 1);
            
                if(Events == null) {
                      Events = new List<Event>();
                      for(Integer i = 0; i < EventNum; i++) {
                            Event e = new Event();
                            e.subject               = 'Test Subject ' + i;
                            e.type                  = 'Email';
                            //e.ActivityDate          = myDate;
                            e.StartDateTime = myDate;
                            e.EndDateTime = myDate.adddays(1); 
                            e.ShowAs = 'Busy';
                            
                            //e.status              = 'In Progress';
                            //e.priority                = 'Normal';
                            
                            Integer j = Math.mod(i,getAccounts(true).size());
                            e.WhatId                = getAccounts(true)[0].Id;
           
                            Events.add(e);
                      }
                      if(xCommit) insert Events;
                }
                
                if(xCommit && Events.size() > 0 && Events[0].Id == null) insert Events;                                            
                
                return Events;
          }
          

/*
    public List<Task> getmpTasks(Boolean xCommit) {
            
                Date myDate = date.newinstance(2013, 1, 15);
            
                if(mpTasks == null) {
                      mpTasks = new List<Task>();
                      for(Integer i = 0; i < mpTaskNum; i++) {
                            Task t = new Task();
                            t.subject               = 'Test Subject ' + i;
                            t.type                  = 'Email';
                            t.ActivityDate          = myDate;
                            t.status                = 'In Progress';
                            t.priority              = 'Normal';
                            t.WhatId                = getmarketingProjects(true)[i].Id;
                            
                            t.Actual_Hours_Worked__c= 5;
                            t.Estimated_Hours__c    = 3;
                            //WhatId
                            //t.Last_Due_Date__c
                            //Original_Due_Date__c
                            //Referral_Source_Rating__c
                            //Referral_Source_Type__c
           
                            mpTasks.add(t);
                      }
                      if(xCommit) insert mpTasks;
                }
                
                if(xCommit && mpTasks.size() > 0 && mpTasks[0].Id == null) insert mpTasks;                                            
                
                return mpTasks;
          }
*/
          
    public List<Campaign> getCampaigns(Boolean xCommit) {
            
                Date myDate = date.newinstance(2013, 1, 15);
                Date endDate = date.newInstance(2013, 1, 17);
                Datetime endDateTime = date.newInstance(2014, 12, 01);
                             
             
            
                if(campaigns == null) {
                      campaigns = new List<Campaign>();
                      for(Integer i = 0; i < campaignNum; i++) {
                            Campaign c = new Campaign();
                            c.Name = 'Test Campaign ' + i; 
                            c.StartDate = myDate;        
                            c.EndDate = endDate;
                            c.End_Date_Time__c = endDateTime;
                            //c.Marketing_Unit__c = 'Sage External';
                            //c.Classification__c = 'Sales';
                            c.Type              = 'Conference';          
                            //c.Marketing_Unit__c = 'Dexter';
    
    
                            campaigns.add(c);
                      }
                      if(xCommit) insert campaigns;
                }
                
                if(xCommit && campaigns.size() > 0 && campaigns[0].Id == null) insert campaigns;                                            
                
                return campaigns;
          }
    
    /*
    public List<Task> getCampaignTasks(Boolean xCommit) {
            
                Date myDate = date.newinstance(2013, 1, 15);
            
                if(campaignTasks == null) {
                      campaignTasks = new List<Task>();
                      for(Integer i = 0; i < campaignTaskNum; i++) {
                            Task t = new Task();
                            t.subject               = 'Test Campaign Tasks ' + i;
                            t.type                  = 'Email';
                            t.ActivityDate          = myDate;
                            t.status                = 'In Progress';
                            t.priority              = 'Normal';
                            t.WhatId                = getCampaigns(true)[i].Id;
                            
                            t.Actual_Hours_Worked__c= 5;
                            t.Estimated_Hours__c    = 3;
                            //WhatId
                            //t.Last_Due_Date__c
                            //Original_Due_Date__c
                            //Referral_Source_Rating__c
                            //Referral_Source_Type__c
           
                            campaignTasks.add(t);
                      }
                      if(xCommit) insert campaignTasks;
                }
                
                if(xCommit && campaignTasks.size() > 0 && campaignTasks[0].Id == null) insert campaignTasks;                                            
                
                return campaignTasks;
          }
   */

  /*
   public List<Clone_Settings__c> getCloneSettings(){
        if (cloneSettings == null){
            //Create Custom Settings for clone
            cloneSettings = new List<Clone_Settings__c>();
            
            //Default Billing City (String)
            Clone_Settings__c settings1 = new Clone_Settings__c();
            settings1.Name = 'TEST Billing Default';
            settings1.Object__c = 'Account';
            settings1.Field_Name__c = 'BillingCity';
            settings1.Default_Value__c = 'Austin';
            cloneSettings.add(settings1);   
            //Default LastTransferDate (Date)
            Clone_Settings__c settings2 = new Clone_Settings__c();
            settings2.Name = 'TEST LastTransferDate Christmas';
            settings2.Object__c = 'Lead';
            settings2.Field_Name__c = 'LastTransferDate';
            settings2.Default_Value__c = '12/25/2020';
            cloneSettings.add(settings2);
            //Default CloseDate (Date TODAY())
            Clone_Settings__c settings3 = new Clone_Settings__c();
            settings3.Name = 'TEST CloseDate TODAY()';
            settings3.Object__c = 'Opportunity';
            settings3.Field_Name__c = 'CloseDate';
            settings3.Default_Value__c = 'TODAY()';
            cloneSettings.add(settings3);
            //Default AnnualRevenue (Currency/Decimal)
            Clone_Settings__c settings4 = new Clone_Settings__c();
            settings4.Name = 'TEST AnnualRevenue Default';
            settings4.Object__c = 'Account';
            settings4.Field_Name__c = 'AnnualRevenue';
            settings4.Default_Value__c = '0';
            cloneSettings.add(settings4);
            //Default AnnualRevenue NULL()
            Clone_Settings__c settings5 = new Clone_Settings__c();
            settings5.Name = 'TEST BillingStreet Default';
            settings5.Object__c = 'Account';
            settings5.Field_Name__c = 'BillingStreet';
            settings5.Default_Value__c = 'NULL()';
            cloneSettings.add(settings5);
            //Default DoNotCall (Boolean)
            Clone_Settings__c settings6 = new Clone_Settings__c();
            settings6.Name = 'TEST DoNotCall Default';
            settings6.Object__c = 'Lead';
            settings6.Field_Name__c = 'DoNotCall';
            settings6.Default_Value__c = 'True';
            cloneSettings.add(settings6);
            //Default NumberOfEmployees (Number)
            Clone_Settings__c settings7 = new Clone_Settings__c();
            settings7.Name = 'TEST NumberOfEmployees Default';
            settings7.Object__c = 'Lead';
            settings7.Field_Name__c = 'NumberOfEmployees';
            settings7.Default_Value__c = '10';
            cloneSettings.add(settings7);
            
            insert cloneSettings;     
        }
        return cloneSettings;
    }  
      */
      /*
      public MarketingProjectPageSetting__c getMpSettings(){
        if (mpSettings == null){
            //Create Custom Settings for clone
            mpSettings = new MarketingProjectPageSetting__c();
            
            //Default Billing City (String)
            MarketingProjectPageSetting__c settings = new MarketingProjectPageSetting__c();
            mpSettings.Name = 'MarketingProjectPageSetting';
            mpSettings.Ignore_Closed_Campaign_RT__c = '012L00000008bAn';   // Archived Request Type
            mpSettings.BannerLogo__c = '069L0000000Cnqy';
               
            
            insert mpSettings;     
        }
        return mpSettings;
    }     
      */
      
      /*

      public MarketingProjectChatterSettings__c getMpChatSettings(){
            if (mpChatSettings == null){
                //Create Custom Settings for clone
                mpChatSettings = new MarketingProjectChatterSettings__c();

                mpChatSettings.Marketing_Chatter_Follow__c = true;              
                mpChatSettings.Auto_Following_a_Campaign__c = true; 
                mpChatSettings.Auto_Follow_a_Marketing_Project__c = true;
                mpChatSettings.Auto_Follow_a_Task__c = true;
                mpChatSettings.Auto_Unassign_Task_Follow__c = true;

                mpChatSettings.Auto_Unfollow_a_Complete_Campaign__c = true;
                mpChatSettings.Auto_Unfollow_a_Complete_Project__c = true;
                mpChatSettings.Auto_Unfollow_a_Complete_Task__c = true;
                
                mpChatSettings.Chatter_Unfollow_From_x_Days_Back__c = 100;
                mpChatSettings.Chatter_Unfollow_x_Days_Grace_Period__c = -1; // To Include the current day
                
                mpChatSettings.Chatter_Unfollow_Nightly_Process_ON__c = true;
                
                mpChatSettings.Chatter_Trigger_Messages__c = true;
                mpChatSettings.Chatter_Trigger_Message_Interval_mins__c = 1;
                
                insert mpChatSettings;     
            }
            return mpChatSettings;
        }     
      */
      
      public Map<String,List<User>> getProfileToUserMap(List<String> profileNames) {
            System.debug('*** getProfileToUserMap');
            getUsers(profileNames);
            return profileToUserMap;
      }
      
      //A user need a profile
      public List<User> getUsers(List<String> profileNames){
            System.debug('*** getUsers');
            if(users == null) {
                  users = new List<User>();
                  
                  //There are 3 classes of users, create at least one of each
                  if(profileNames == null || profileNames.size() == 0) {
                        profileNames = new List<String>();
                        profileNames.add('Standard User');        
                  }
                  
                  System.debug('*** profileNames: ' + profileNames);
                  
                  Map<Id,Profile> profileMap;               
                  try {       
                        profileMap = new Map<Id,Profile>([select id, Name from profile where name IN :profileNames]);
                  } catch (Exception exp) {
                        //Can not test exceptions
                        System.debug('*** Could not find profiles: ' + exp.getMessage());                        
                  }
                  
                  if(profileMap == null || profileMap.size() == 0) {
                        profileMap = new Map<Id,Profile>([select id, Name from profile where name = 'Standard User']);
                  }
                  
                  System.debug('*** profileMap: ' + profileMap);
                  
                  //Untested - this should never happen, just 3x checking
                  if(profileMap == null || profileMap.size() == 0) {
                        System.assert(false,'Could not find profiles');
                        return null;
                  }
                                                      
                  if(userNum == null || userNum < profileMap.size()) {
                        userNum = profileMap.size();
                  }
                              
                  for(Integer i = 0; i < userNum; i++) {
                        Integer profilePosition = Math.mod(i,profileMap.size());
                        Integer random = Math.random().intValue();
                        User u = getUser(profileMap.values()[profilePosition], 'User' + i.format() + random.format());
                        users.add(u);                                   
                  }
                  
                  System.debug('*** inserting users');
                  insert users;
                  
                  //Now build the user map
                  profileToUserMap = new Map<String,List<User>>();
                  
                  for(User u : users) {
                        if(profileToUserMap.containsKey(profileMap.get(u.ProfileId).Name)) {
                              profileToUserMap.get(profileMap.get(u.ProfileId).Name).add(u);
                        } else {
                              List<User> tmpUsers = new List<User>();
                              tmpUsers.add(u);
                              profileToUserMap.put(profileMap.get(u.ProfileId).Name,tmpUsers);
                        }
                  }
            }           
            return users;
      }
      
      public User getUser(Profile p, String uniqueName){
            System.debug('*** getUser');
                        
            String orgId = (String)Userinfo.getOrganizationId();        
            String uniqueEmail = uniqueName + '@' + orgId + '.net.test';
            
            User u = new User(alias = 'standt', 
                              email=uniqueEmail,
                              emailencodingkey='UTF-8', 
                              lastname='Testing', 
                              languagelocalekey='en_US',
                              localesidkey='en_US', 
                              profileid = p.Id,
                              timezonesidkey='America/Los_Angeles',
                              username=uniqueEmail,
                              isActive=true);
            return u;
      }
      
      
            
      public List<Account> getAccounts(Boolean xCommit) {
            if(accounts == null) {
                  accounts = new List<Account>();
                  for(Integer i = 0; i < accountNum; i++) {
                        Account a = new Account();
                        a.Name = 'Test ' + i;   
                        a.BillingState = 'Texas';       
                        a.BillingCountry = 'United States';   
                        accounts.add(a);
                  }
                  if(xCommit) insert accounts;
            }
            
            if(xCommit && accounts.size() > 0 && accounts[0].Id == null) insert accounts;                                            
            
            return accounts;
      }
      
            
                  
      public List<Contact> getContacts(Boolean xCommit) {
            if(contacts == null) {
                  contacts = new List<Contact>();
                  for(Integer i = 0; i < contactNum; i++) {
                        Contact c = new Contact();
                        c.FirstName = 'First ' + i;
                        c.LastName = 'Last ' + i;
                        c.MailingState = 'Texas';
                        c.MailingCountry ='United States';
                       // c.Sage_Rapport__c = getusers(null)[0].Id;
                        Integer j = Math.mod(i,getAccounts(true).size());
                        c.Account = getAccounts(true)[0];
                        //c.OwnerId = getusers(null)[0].Id;
                        
                        contacts.add(c);
                  }
                  if(xCommit) insert contacts;
            }
            
            if(xCommit && contacts.size() > 0 && contacts[0].Id == null) insert contacts;
            
            return contacts;
      }
      
      
  /*    
      public Pricebook2 getPricebook(Boolean xCommit) {
            if(pricebook == null) {
                  pricebook = new Pricebook2();
                  pricebook.IsActive = true;
                  pricebook.Name = 'Test Pricebook';
                  if(xCommit) insert pricebook;
            }
            
            if(xCommit && pricebook.Id == null) insert pricebook;                         
            
            return pricebook;
      }
      
      
      
      public List<Product2> getProducts(Boolean xCommit) {
            if(products == null) {
                  products = new List<Product2>();
                  for(Integer i = 0; i < productNum; i++) {
                        Product2 p = new Product2();
                        p.IsActive = true;
                        p.Name = 'Test Product ' + i;
                        p.ProductCode = 'TestPCode ' + i; 
                        products.add(p);
                  }
                  if(xCommit) insert products;
            }
            
            if(xCommit && products.size() > 0 && products[0].Id == null) insert products;              
            
            return products;
      }
      
      
      
      public List<PricebookEntry> getPricebookEntries(Boolean xCommit) {
            if(pricebookEntries == null) {
                  pricebookEntries = new List<PricebookEntry>();
                  List<PricebookEntry> stdPricebookEntries = new List<PricebookEntry>();
                  
                  Pricebook2 standardPB = [SELECT Id FROM Pricebook2 WHERE IsStandard = true LIMIT 1];
                  for(Integer i = 0; i < pricebookEntryNum; i++) {
                        PricebookEntry pbeStd = new PricebookEntry();
                        pbeStd.IsActive = true;
                        pbeStd.Pricebook2Id = standardPB.Id;
                        System.assertNotEquals(null, pbeStd.Pricebook2Id,standardPB);
                        
                        Integer j = Math.mod(i, getProducts(true).size());
                        pbeStd.Product2Id = getProducts(true)[j].Id;
                        System.assertNotEquals(null, pbeStd.Product2Id,getProducts(true)[j]);
                        
                        pbeStd.UnitPrice = i;
                        pbeStd.UseStandardPrice = false;
                        stdPricebookEntries.add(pbeStd);
                        
                        PricebookEntry pbe = new PricebookEntry();
                        pbe.IsActive = true;
                        pbe.Pricebook2Id = getPricebook(true).Id;
                        System.assertNotEquals(null, pbe.Pricebook2Id,getPricebook(true));
                                                
                        pbe.Product2Id = pbeStd.Product2Id;
                        System.assertNotEquals(null, pbe.Product2Id,pbeStd);
                        
                        pbe.UnitPrice = i;
                        pbe.UseStandardPrice = false;
                        pricebookEntries.add(pbe);
                  }
                  insert stdPricebookEntries;
                  if(xCommit) insert pricebookEntries;
            }
            
            if(xCommit && pricebookEntries.size() > 0 && pricebookEntries[0].Id == null) insert pricebookEntries;          
            
            return pricebookEntries;
      }
      */
      
      
      public List<Opportunity> getOpportunities(Boolean xCommit) {
            if(opportunities == null) {
                  opportunities = new List<Opportunity>();
                  for(Integer i = 0; i < opptyNum; i++) {
                        Opportunity o = new Opportunity();
                        o.Name = 'Test ' + i;
                        o.CloseDate = Date.today().addDays(1);
                        o.StageName = '01-New';
                        Integer j = Math.mod(i,getAccounts(true).size());
                        o.Account = getAccounts(true)[0];
                      	o.Prospect_Outlook__c = 'Hot';
                        //o.Pricebook2Id = getPricebook(true).Id;
                        opportunities.add(o);
                  }
                  if(xCommit) insert opportunities;
            }
            
            if(xCommit && opportunities.size() > 0 && opportunities[0].Id == null) insert opportunities;
            
            return opportunities;
      }
      
     
      /*
      public List<OpportunityLineItem> getOpportunityLineItems(Boolean xCommit) {
            if(opportunityLineItems == null) {
                  opportunityLineItems = new List<OpportunityLineItem>();
                  for(Integer i = 0; i < opptyLineItemNum; i++) {
                        OpportunityLineItem oli = new OpportunityLineItem();
                        
                        Integer j = Math.mod(i,getOpportunities(true).size());
                        oli.OpportunityId = getOpportunities(true)[j].Id;
                        
                        Integer k = Math.mod(i,getPricebookEntries(true).size());                     
                        oli.PricebookEntryId = getPricebookEntries(true)[k].Id;
                        
                        oli.Quantity = i + 1;
                        
                        oli.UnitPrice = getPricebookEntries(true)[k].UnitPrice;
                                          
                        opportunityLineItems.add(oli);                        
                  }
                  if(xCommit) insert opportunityLineItems;
            }
            
            if(xCommit && opportunityLineItems.size() > 0 && opportunityLineItems[0].Id == null) insert opportunityLineItems;
                        
            return opportunityLineItems;
      }
      */
      
      public List<Lead> getLeads(Boolean xCommit) {
            if(leads == null) {
                  leads = new List<Lead>();
                  for(Integer i = 0; i < leadNum; i++) {
                        Lead lead = new Lead();
                        lead.FirstName = 'First ' + i;
                        lead.LastName = 'Last ' + i;
                        lead.Email = 'email' + i + '@test.com';
                        lead.Phone = '555-555-555' + i;
                        lead.Company = 'Test Company' + i;
                        lead.State ='Texas';
                        lead.Country='United States';
                  
                        //lead.Accept_Lead_Date__c=system.today()-34;
                        
                  //      Integer j = Math.mod(i,getAccounts(true).size());
                  //      Account a = getAccounts(true)[j];                     
                        //lead.Company = a.Name;
                        
                        lead.Company = 'First ' + i;
                        
                        leads.add(lead);
                  }
                  if(xCommit) insert leads;
            }
            
            if(xCommit && leads.size() > 0 && leads[0].Id == null) insert leads;
            
            return leads;
      }
      
     
    //public List<Lead> convertLeads(List<Lead> leads) {
    public static void convertLeads(List<Lead> leads) {
        Database.LeadConvert lc = new database.LeadConvert();
        lc.setLeadId(leads[0].Id);
        
        LeadStatus convertStatus = [SELECT Id, MasterLabel FROM LeadStatus WHERE IsConverted=true LIMIT 1];
        lc.setConvertedStatus(convertStatus.MasterLabel);
        
        Database.LeadConvertResult lcr = Database.convertLead(lc);
        System.assert(lcr.isSuccess());
    }
    
    /*
    public List<Booth__c> getBooths( Boolean xCommit )
    {
        if(booths == null) {
                  booths = new List<Booth__c>();
                  for(Integer i = 0; i < boothNum; i++) {
                        Booth__c b = new Booth__c();
                        b.Name = 'Booth ' + String.valueOf( i );
                        b.Status__c = 'Active';
                               
                        booths.add(b);
                  }
                  if(xCommit) insert booths;
            }
            
            if(xCommit && booths.size() > 0 && booths[0].Id == null) insert booths;                                            
            
            return booths;  
    }
    */
 
      public void initAllTestData() {
            
            //getMpChatSettings();
           // getCloneSettings();
           // getMpSettings();
            
            getUsers(null);
            getLeads(true);
            //convertLeads(getLeads(true));  // Get and Convert new leads. 
            getAccounts(true);
            getContacts(true);
            
            getCampaigns(true);
            //getCampaignTasks(true);  // Try this.
            
          //  getmarketingProjects(true);
            //getmpTasks(false);
            
            //getmpTasks(true);
            getTasks(true);
            getEvents(true);
           // getCosts(true);
            
            getOpportunities(true);
            
            /*
            getPricebook(true);
            getProducts(true);
            
            getOpportunityLineItems(true);
            */
            
      }
 
     static testMethod void TestDataGeneratorUnitTest() {
        TestDataGenerator data = new TestDataGenerator();
        data.initAllTestData();        
        //System.assertEquals(data.accountNum, data.getAccounts(true).size(),'Account size not equal to accountNum');
        //System.assertNotEquals(null,data.getOpportunityLineItems(true)[0].Id);                           
    }
 
}