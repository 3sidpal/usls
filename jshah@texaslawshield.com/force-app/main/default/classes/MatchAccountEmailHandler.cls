/**  
* ─────────────────────────────────────────────────────────────────────────────────────────────────┐
* @author         Unmesha Punyamurthula
* @version        1.0
* @created        06/04/2019
* @TestClass      MatchAccountEmailHandlerTest
* @Trigger        AccountTriggerAfter
* @Description    This Class is used to find the duplicate Contact Email's 
* ─────────────────────────────────────────────────────────────────────────────────────────────────┘
*/

public class MatchAccountEmailHandler {
    
    public static void findDuplicateEmail(List<Account> newAccList,Map<id,Account> oldMap){
        if(newAccList != null && newAccList.size() > 0){
            Set<String> emailSet = new Set<string>();
            Set<String> oldEmailSet = new Set<string>();
             Set<String> AllEmailSet = new Set<string>();
            for(Account acc: newAccList){
                if(Trigger.isinsert){
                    if(acc.PersonEmail != null ){
                        emailSet.add(acc.PersonEmail );
                        system.debug('@@accPersonEmail'+acc.PersonEmail);
                        //emailSet.add(acc.Secondary_Email__c);SecondaryEmailAddress__c Secondary_Email__c
                    }
                    if( acc.SecondaryEmailAddress__c !=null){
                       
                        system.debug('@@SecondaryEmailAddress__c'+acc.SecondaryEmailAddress__c);
                        
                        emailSet.add(acc.SecondaryEmailAddress__c);
                        
                    }
                }else if(Trigger.isUpdate){
                    Account oldacc = oldMap.get(acc.Id);
                    if(acc.PersonEmail != oldacc.PersonEmail){
                        if(acc.PersonEmail != null){
                        	emailSet.add(acc.PersonEmail);
                            //emailSet.add(acc.SecondaryEmailAddress__c);
                        }
                        
                        if( acc.SecondaryEmailAddress__c !=null){
                       
                        //system.debug('@@SecondaryEmailAddress__c'+acc.SecondaryEmailAddress__c);
                        
                        emailSet.add(acc.SecondaryEmailAddress__c);
                        
                    }
                        
                        if(oldacc.PersonEmail != null ){
                            oldEmailSet.add(oldacc.PersonEmail);
                           
                        }
                        if(oldacc.SecondaryEmailAddress__c != null ){
                            oldEmailSet.add(oldacc.SecondaryEmailAddress__c);
                           
                        }
                    }
                }
                else if(Trigger.isDelete){
                    if(acc.PersonEmail != null ){
                        oldEmailSet.add(acc.PersonEmail);   
                    }
                    if(acc.SecondaryEmailAddress__c != null ){
                        oldEmailSet.add(acc.SecondaryEmailAddress__c);   
                    }
                }
            }
            if(emailSet.size() > 0){
            	AllEmailSet.addAll(emailSet);
            }
            if(oldEmailSet.size() > 0){
            	AllEmailSet.addAll(oldEmailSet);
            }
            //system.debug('@@emailSet'+emailSet);
            //system.debug('@@oldEmailSet'+oldEmailSet);
           // system.debug('@@AllEmailSet'+AllEmailSet);
            
            if(AllEmailSet.size() > 0){
                List<Contact> conList = [Select Id ,Duplicate_Contact__c,  Email FROM COntact WHERE email IN : AllEmailSet and isPersonAccount = False];
                system.debug('@@conList'+conList);
                if(conLIst.size() > 0){
                     for(Contact con: conList){
                         
                         //system.debug('@@@con'+con);
                        if(emailSet.contains(con.Email)){
                            con.Duplicate_Contact__c = true;
                           con.Secondary_Duplicate_Contact__c = True;
                            system.debug('@@@true'+con);
                        }else if(oldEmailSet.contains(con.Email)){
                            con.Duplicate_Contact__c = false;
                            system.debug('@@false');
                        }
                         system.debug('@@end');
                    }
                    
                    update conList; 
                }
                
            }
        }
    }
}