@istest
public with sharing class SubscriptionProductChargeTestClone {

    @IsTest
    public static void testInsert1() {
        //
        List<Zuora__SubscriptionProductCharge__c> subList = createSubscriptions();

        System.Test.startTest();
            insert sublist;
        System.Test.stopTest();

        List<Account> accountQueryList = [
            SELECT Id
                 , Member_Type__c
                 , Primary__c
              FROM Account
             WHERE LastName = :'Sony inc'
        ];

        List<Account> accountQueryList2 = [
            SELECT Id
                 , Member_Type__c
                 , Primary__c
              FROM Account
             WHERE LastName = :'Lawshield'
        ];

        System.assertEquals(null, accountQueryList.get(0).Member_Type__c);
        System.assertEquals(null, accountQueryList2.get(0).Member_Type__c);
        System.assertEquals(false, accountQueryList.get(0).Primary__c);
        System.assertEquals(false, accountQueryList2.get(0).Primary__c);
        //
    }

    @IsTest
    public static void testUpdate2() {
        //
        List<Zuora__SubscriptionProductCharge__c> subList = createSubscriptions();
        insert sublist;

        System.Test.startTest();
            update sublist;
        System.Test.stopTest();

        List<Account> accountQueryList = [
            SELECT Id
                 , Member_Type__c
                 , Primary__c
              FROM Account
             WHERE LastName = :'Sony inc'
        ];

        List<Account> accountQueryList2 = [
            SELECT Id
                 , Member_Type__c
                 , Primary__c
              FROM Account
             WHERE LastName = :'Lawshield'
        ];

        System.assertEquals('Annual', accountQueryList.get(0).Member_Type__c);
        System.assertEquals('Monthly', accountQueryList2.get(0).Member_Type__c);
        System.assertEquals(false, accountQueryList.get(0).Primary__c);
        System.assertEquals(false, accountQueryList2.get(0).Primary__c);
        //
    }

    @IsTest
    public static void testDelete3() {
        //
        List<Zuora__SubscriptionProductCharge__c> subList = createSubscriptions();
        insert sublist;

        System.Test.startTest();
            delete sublist.get(7);
            delete sublist.get(4);
            delete sublist.get(6);
            delete sublist.get(1);
        System.Test.stopTest();

        List<Account> accountQueryList = [
            SELECT Id
                 , Member_Type__c
                 , Primary__c
              FROM Account
             WHERE LastName = :'Sony inc'
        ];

        List<Account> accountQueryList2 = [
            SELECT Id
                 , Member_Type__c
                 , Primary__c
              FROM Account
             WHERE LastName = :'Lawshield'
        ];

        System.assertEquals('Annual', accountQueryList.get(0).Member_Type__c);
        System.assertEquals('Monthly', accountQueryList2.get(0).Member_Type__c);
        System.assertEquals(false, accountQueryList.get(0).Primary__c);
        System.assertEquals(false, accountQueryList2.get(0).Primary__c);
        //
    }


    public static List<Zuora__SubscriptionProductCharge__c> createSubscriptions() {
        //
        List<Account> accountsListToUpdate = new list<Account>();
        List<Zuora__SubscriptionProductCharge__c> subList = new List<Zuora__SubscriptionProductCharge__c>();
        Account acc = new Account();
        acc.LastName = 'Sony inc';
        acc.PersonEmail = 'test123@gmail.com';
        accountsListToUpdate.add(acc);

        Account acc2 = new Account();
        acc2.lastName ='Lawshield';
        acc2.PersonEmail = 'lawshield@gmail.com';
        accountsListToUpdate.add(acc2);

        Account acc3 = new Account();
        acc3.lastname = 'Lawshield2';
        acc3.PersonEmail = 'texaslawshield@gmail.com';
        accountsListToUpdate.add(acc3);

        Account acc4 = new Account();
        acc4.lastname = 'Lawshield3';
        acc4.PersonEmail ='lawshield3@gmail.com';
        accountsListToUpdate.add(acc4);

        insert accountsListToUpdate;

        ////////////////////////////////////////////////////////////////

        Zuora__SubscriptionProductCharge__c sub = new Zuora__SubscriptionProductCharge__c();
        sub.Zuora__Account__c = acc.Id;
        sub.name = 'Annual Sub';
        sub.Zuora__Zuora_Id__c ='12345678';
        sublist.add(sub);

        Zuora__SubscriptionProductCharge__c subscription = new Zuora__SubscriptionProductCharge__c();
        subscription.Zuora__Account__c = acc.Id;
        subscription.name = 'Test Data';
        subscription.Zuora__Zuora_Id__c ='17345678';
        sublist.add(subscription);

        Zuora__SubscriptionProductCharge__c subTestData = new Zuora__SubscriptionProductCharge__c();
        subTestData.Zuora__Account__c = acc.Id;
        subTestData.name = 'Test Data8';
        subTestData.Zuora__Zuora_Id__c ='17345678';
        sublist.add(subTestData);

        Zuora__SubscriptionProductCharge__c sub2 = new Zuora__SubscriptionProductCharge__c();
        sub2.Zuora__Account__c = acc2.Id;
        sub2.name = 'Monthly Sub';
        sub2.Zuora__Zuora_Id__c ='1234de4';
        sublist.add(sub2);

        Zuora__SubscriptionProductCharge__c sub12 = new Zuora__SubscriptionProductCharge__c();
        sub12.Zuora__Account__c = acc2.Id;
        sub12.name = 'Test Data 2';
        sub12.Zuora__Zuora_Id__c ='12377de4';
        sublist.add(sub12);

        Zuora__SubscriptionProductCharge__c subTest = new Zuora__SubscriptionProductCharge__c();
        subTest.Zuora__Account__c = acc2.Id;
        subTest.name = 'Test Data8';
        subTest.Zuora__Zuora_Id__c ='1op77de4';
        sublist.add(subTest);

        Zuora__SubscriptionProductCharge__c sub3 = new Zuora__SubscriptionProductCharge__c();
        sub3.Zuora__Account__c = acc3.Id;
        sub3.name = 'Diamond Sub';
        sub3.Zuora__Zuora_Id__c ='1234ghty5678';
        sublist.add(sub3);

        Zuora__SubscriptionProductCharge__c sub7 = new Zuora__SubscriptionProductCharge__c();
        sub7.Zuora__Account__c = acc3.Id;
        sub7.name = 'Testing';
        sub7.Zuora__Zuora_Id__c ='1Testghty5678';
        sublist.add(sub7);

        Zuora__SubscriptionProductCharge__c sub4 = new Zuora__SubscriptionProductCharge__c();
        sub4.Zuora__Account__c = acc4.Id;
        sub4.name = 'Other Sub';
        sub4.Zuora__Zuora_Id__c ='12bncde345678';
        sublist.add(sub4);

        return sublist;
        //
    }
}