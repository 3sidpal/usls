// This test class is for the Apex Class UpdatePrimaryOnAccount

@isTest
public class UpdatePrimaryOnAccountTest {
    
     static testmethod void  testPrimaryData(){
     
     Account acc = new Account();
     acc.Name = 'Test Texas lawshield';
     //acc.Email = 'test@gmail.com';
     acc.Subscription_Start_Date__c = system.today();
     
     insert acc;
     
     Zuora__Subscription__c zsb = new Zuora__Subscription__c();
     zsb.Name = 'Test Main Subsrition';
     zsb.Zuora__Status__c = 'Active';
     zsb.Zuora__SubscriptionStartDate__c =system.Today();
     Zsb.Zuora__Account__c =  acc.id;
     insert zsb;
     
     
     test.starttest();
     Zuora__SubscriptionProductCharge__c zsub = new Zuora__SubscriptionProductCharge__c();
     
     zsub.Name = 'Test Subscription';
     zsub.Zuora__Zuora_Id__c = 'test12334555';
     zsub.Zuora__RatePlanName__c = 'Individual Plan';
     zsub.Zuora__Account__c = acc.id;
     zsub.Zuora__Subscription__c = zsb.id;
     
     insert zsub;
     test.stoptest();
     
     //system.assertEquals(TRUE, acc.Primary__c);
     
     
     }
}