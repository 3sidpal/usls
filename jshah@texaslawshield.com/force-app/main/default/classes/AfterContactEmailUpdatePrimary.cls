/**  
* ─────────────────────────────────────────────────────────────────────────────────────────────────┐
* @author         Unmesha Punyamurthula
* @version        1.0
* @created        06/04/2019
* @TestClass      AfterContactEmailUpdateTest
* @Trigger        ContactTrigger
* @Description    This Class is used to find the duplicate Contact Email's 
* ─────────────────────────────────────────────────────────────────────────────────────────────────┘
*/
public class AfterContactEmailUpdatePrimary{
    
    public static void beforeUpDateMethod(List<Contact> newConList, Map<id, Contact> oldConMap)
        
    {
        
        List<Contact> contList = new List<Contact>();
        Map<string, id> conMap = new Map<string, id>();
        Map<String, boolean> AccMap = new Map<string, boolean>();
        
        for(Contact con: newConList){
            if(con.email != null){
                conMap.put(con.email,con.id);
            }else{
                con.Secondary_Duplicate_Contact__c = false;
            }
        }
        List<Account> AccSearch = [Select id, SecondaryEmailAddress__c from Account where SecondaryEmailAddress__c in:conMap.keyset()];
        
        if(AccSearch.size()>0){
            for(Account acc: AccSearch){
                accMap.put(acc.SecondaryEmailAddress__c, true);
            }
        }
       // system.debug('@@27'+accMap);
        for(Contact con: newConList){
            if(con.email != null){
                if(accMap.containsKey(con.email)){
                    con.Secondary_Duplicate_Contact__c = true;
                }else{
                    con.Secondary_Duplicate_Contact__c = false;
                }
            }else{
                con.Secondary_Duplicate_Contact__c = false;
            }
        }
        
        
    }
    
    
}