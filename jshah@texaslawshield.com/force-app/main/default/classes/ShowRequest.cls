public class ShowRequest 
{ 
     //---------------------------------------------------------------------------------
    // Private Properties
    //---------------------------------------------------------------------------------
    private static Id rtCaseWorkshop = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Event Request (Workshop)').getRecordTypeId();
    private static Id rtCaseSeminar = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Event Request (Seminar)').getRecordTypeId();
        
    
    //---------------------------------------------------------------------------------
    // Public Properties
    //---------------------------------------------------------------------------------
    // the number of new attachments to add to the list when the user clicks 'Add More'
    public static final Integer NUM_ATTACHMENTS_TO_START = 3;
    public static final Integer NUM_ATTACHMENTS_TO_ADD = 1;
    
    @TestVisible
    private Case objCase;
    
    public string strCaseNumber
    {
        get
        {
            if (objCase.Id != null)
            {
                    Case c = [SELECT CaseNumber FROM Case WHERE Id = :objCase.Id];
                    return c.CaseNumber;
            }
            else
            {
                    return '';
            }
        }
    }
    
    public string strCaseType
    {
        get
        {
            if (objCase.Id != null)
            {
                    return objCase.Type;
            }
            else
            {
                    return '';
            }
        }
    }
    
    public string strReqName 
    {
        get
        {
            if (userInfo.getUserId() != null)
            {
                return userInfo.getName();
            }
            else
            {
                return '';
            }
        }
        set;
    }
    
    public string strReqEmail 
    {
        get
        {
            if (userInfo.getUserId() != null)
            {
                return userInfo.getUserEmail();
            }
            else
            {
                return '';
            }
        } 
        set;
    }
    
    public string strEventType {get; set;}
    
    public list<SelectOption> lstEventTypes
    {
        get
        {
            list<SelectOption> options = new list<SelectOption>();
            
            Schema.DescribeFieldResult fieldResult = evt__Special_Event__c.evt__Event_Type__c.getDescribe();
            list<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
            
            options.add(new SelectOption('', ''));
            for(Schema.PicklistEntry f : ple)
            {
                options.add(new SelectOption(f.getLabel(), f.getValue()));
            }   
            
            return options;
        }
    }
    
    public String fetchPromoterPhone
    {
        get
        {
           if (objCase.Promoter_Name__c != null)
            {
                Account objAcc = [SELECT Phone FROM Account WHERE Id=: objCase.Promoter_Name__c] ;
                return objAcc.Phone;
            }
            
            return null;
        }
        set;
    }
    
    public list<SelectOption> lstFacilityContacts
    {
        get
        {
            system.debug('*** sgi_EventRequest_Controller | lstFacilityContacts - start');
            
            list<SelectOption> options = new list<SelectOption>();
            options.add(new SelectOption('', ' '));
            
            system.debug('*** sgi_EventRequest_Controller | lstFacilityContacts - objCase.Host_Facility__c: ' + objCase.Promoter_Name__c);
            
            if (objCase.Promoter_Name__c != null)
            {
                for(Contact c : [SELECT Id, Name FROM Contact WHERE AccountId = :objCase.Promoter_Name__c])
                {
                    options.add(new SelectOption(c.Id, c.Name));
                }   
            }
            
            return options;
        }
    } 
    
    //public evt__Special_Event__c objEvent{get; set;}
    public string strFacilityContact{get; set;}
    
    // list of new attachments to add
    public List<Attachment> newAttachments {get; set;}
    
    //---------------------------------------------------------------------------------
    // Public Methods
    //---------------------------------------------------------------------------------
    
    public ShowRequest(ApexPages.StandardController stdController) 
    {
        
        if(!Test.isRunningTest()) stdController.addFields(new List<String>{'Type'});
        objCase = (Case)stdController.getRecord();
        
        objCase.RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Campaign Show Request').getRecordTypeId();
        
        // instantiate the list with attachments
        newAttachments = new List<Attachment>();
        for (integer x = 0; x < NUM_ATTACHMENTS_TO_START; x++)
        {
            newAttachments.add(new Attachment());
        }
        if(ApexPages.currentPage().getParameters().containsKey('reqName'))
        {
            strReqName = ApexPages.currentPage().getParameters().get('reqName');
        }
        if(ApexPages.currentPage().getParameters().containsKey('reqEmail'))
        {
            strReqEmail= ApexPages.currentPage().getParameters().get('reqEmail');
        }
        if(ApexPages.currentPage().getParameters().containsKey('promoterId'))
        {
            objCase.Promoter_Name__c = ApexPages.currentPage().getParameters().get('promoterId');
        }
        if(ApexPages.currentPage().getParameters().containsKey('promoterContact'))
        {
            strFacilityContact = ApexPages.currentPage().getParameters().get('promoterContact');
        }
        if(ApexPages.currentPage().getParameters().containsKey('promoterPhone') )
        {
            fetchPromoterPhone= ApexPages.currentPage().getParameters().get('promoterPhone');
        }
        if(ApexPages.currentPage().getParameters().containsKey('showName') )
        {
            objCase.Show_Name__c= ApexPages.currentPage().getParameters().get('showName');
        }
        if(ApexPages.currentPage().getParameters().containsKey('showType') )
        {
            objCase.Type= ApexPages.currentPage().getParameters().get('showType');
        }
        if(ApexPages.currentPage().getParameters().containsKey('state') )
        {
            objCase.Venue_State__c= ApexPages.currentPage().getParameters().get('state');
        }
    }

    // Add more attachments action method
    public void addMore()
    {
        // append NUM_ATTACHMENTS_TO_ADD to the new attachments list
        for (Integer idx = 0; idx < NUM_ATTACHMENTS_TO_ADD; idx++)
        {
            newAttachments.add(new Attachment());
        }
    } 
    
    public pageReference SaveRequest()
    {
        pageReference pr = null;
        
        // Save the new Case record
        // objCase.RecordTypeId = strEventType == 'Seminar' ? rtCaseSeminar : rtCaseWorkshop;
        // objCase.Type = strEventType;
        objCase.Origin = 'Web (Public)';
        objCase.Reason = 'Show Request';
        objCase.Priority = 'Medium';
        objCase.Requestor_Name__c = strReqName;
        objCase.Requestor_Email__c = strReqEmail;
        objCase.Host_Facility_Contact__c = strFacilityContact;
        
        insert(objCase);
        
        // Now save the attachments to the case
        List<Attachment> toInsert = new List<Attachment>();
        for (Attachment newAtt : newAttachments)
        {
            if (newAtt.Body != null)
            {
                newAtt.parentId = objCase.Id;
                toInsert.add(newAtt);
            }
        }
        
        insert toInsert;
        
        newAttachments.clear();
        newAttachments.add(new Attachment());
        
        return null;
    }   
    public pageReference cloneData()
    {
        pageReference pg = page.showrequest;
        pg.getParameters().put('reqName', strReqName);
        pg.getParameters().put('reqEmail', strReqEmail);
        if(objCase.Promoter_Name__c != null )
        {
            pg.getParameters().put('promoterId', objCase.Promoter_Name__c);
        }
        
        if(String.isNotBlank(strFacilityContact) )
        {
            pg.getParameters().put('promoterContact', strFacilityContact);
        }
        if(String.isNotBlank(fetchPromoterPhone) )
        {
            pg.getParameters().put('promoterPhone', fetchPromoterPhone);
        }
        if(String.isNotBlank(objCase.Show_Name__c) )
        {
            pg.getParameters().put('showName', objCase.Show_Name__c);
        }
        if(String.isNotBlank(objCase.Type) )
        {
            pg.getParameters().put('showType', objCase.Type);
        }
        if(String.isNotBlank(objCase.Venue_State__c) )
        {
            pg.getParameters().put('state', objCase.Venue_State__c);
        }
        pg.setredirect(true);
        return pg;
    }
}