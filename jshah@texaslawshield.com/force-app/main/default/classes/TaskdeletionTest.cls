@isTest
private class TaskdeletionTest {
    
    @isTest static void testinsertuser() {

        Profile pf= [Select Id from profile where Name='*TLS: Members Services Rep']; 
        String orgId=UserInfo.getOrganizationId(); 
        User uu=new User(firstname = 'ABC', 
                         lastName = 'XYZ', 
                         email =  'uniqueName@test' + orgId + '.org', 
                         Username = 'uniqueName@test' + orgId + '.org', 
                         EmailEncodingKey = 'ISO-8859-1', 
                         Alias = 'ABC', 
                         TimeZoneSidKey = 'America/Los_Angeles', 
                         LocaleSidKey = 'en_US', 
                         LanguageLocaleKey = 'en_US', 
                         ProfileId = pf.Id
                        ); 
        
        insert uu;
        
        system.runas(uu){
            Account acct = new Account(Name = 'Test Account');
            insert acct;
            Contact ct = new Contact(AccountId = acct.Id, LastName = 'TestCT');
            insert ct;
            
            Task newTask = new Task(Description = 'GSW Email',
                                    Priority = 'Normal',
                                    Status = 'Completed',
                                    Subject = 'GSW',
                                    Type = 'Email',
                                    WhoId = ct.Id);
            
            
            insert newTask;
            
            try {
                delete newTask;
            } catch (Exception e) {
                System.assert(e.getMessage().contains('delete this record'));
            }
            
        }
    }
}