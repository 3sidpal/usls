@isTest
public class TestDataFactory {
    public static List<Account> getAccounts(Integer numAccounts, boolean isInsert){
       
        List<Account> lstAccount = new List<Account>();
        for(Integer count = 0;count<numAccounts;count++){
            Account objAccount = new Account();
            objAccount.Name = 'Test Texas lawshield'+count;
            lstAccount.add(objAccount);
        }
        if(isInsert){
            insert lstAccount;
        }
        return lstAccount;
    }
    
    public static List<Zuora__Subscription__c> getZuoraSubscriptions(Integer num, boolean isInsert){
        
        List<Zuora__Subscription__c> lstZuoraSubscription = new List<Zuora__Subscription__c>();
        ID accountId = getAccounts(1,true)[0].id;

        for(Integer count = 0;count<num;count++){
            Zuora__Subscription__c objSubscription = new Zuora__Subscription__c();
            objSubscription.Name = 'Test Main Subscription'+count;
            objSubscription.Zuora__Status__c = 'Active';
            objSubscription.Zuora__Account__c =  accountId;
            lstZuoraSubscription.add(objSubscription);
        }
        if(isInsert){
            insert lstZuoraSubscription;
        }
        return lstZuoraSubscription;
    }
    
    public static List<Zuora__SubscriptionProductCharge__c> 
                                getZuoraSubscriptionProductCharge(Integer num, boolean isInsert){
        
        List<Zuora__SubscriptionProductCharge__c> lstZuoraSubscriptionProductCharge 
                                                = new List<Zuora__SubscriptionProductCharge__c>();
        ID accountId = getAccounts(1,true)[0].id;
        ID subscriptionId = getZuoraSubscriptions(1,true)[0].id;

        for(Integer count = 0;count<num;count++){
            Zuora__SubscriptionProductCharge__c objSubscriptionProductCharge 
                                                        = new Zuora__SubscriptionProductCharge__c();
            objSubscriptionProductCharge.Name = 'Test Subscription'+count;
            objSubscriptionProductCharge.Zuora__RatePlanName__c = 'Individual Plan';
            objSubscriptionProductCharge.Zuora__Account__c = accountId;
            objSubscriptionProductCharge.Zuora__Subscription__c = subscriptionId;
            lstZuoraSubscriptionProductCharge.add(objSubscriptionProductCharge);
        }
        if(isInsert){
            insert lstZuoraSubscriptionProductCharge;
        }
        return lstZuoraSubscriptionProductCharge;
    }
}