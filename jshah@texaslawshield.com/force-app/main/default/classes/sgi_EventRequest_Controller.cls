/*-----------------------------------------------------------------------------------------------------------------------------
*   Author:         Strategic Growth, Inc. (www.strategicgrowthinc.com)
*   Description:    This class is used as the custom controller extension class for the EventRegister VF page to allow Case creation
*                   for chatter users and others as well as attachments onto the new case.
*
*   History:
*       Date            Developer       Comments
*       ----------      --------------  ---------------------------------------------------------------------------
*       4/4/2016       	CB              - Initial development
*       7/27/2016       CB              - Updates to support requested changes to the front end as well as moving the page to use the 
*                                           Special Event object instead of the Event Request Template Object
*		11/11/2016		CB				- Change to run off the Case object directly since not all users have license to the Special Event Linvio object
/*-----------------------------------------------------------------------------------------------------------------------------*/
public without sharing class sgi_EventRequest_Controller 
{
    //---------------------------------------------------------------------------------
    // Private Properties
    //---------------------------------------------------------------------------------
    private static Id rtCaseWorkshop = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Event Request (Workshop)').getRecordTypeId();
    private static Id rtCaseSeminar = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Event Request (Seminar)').getRecordTypeId();
        
    
    //---------------------------------------------------------------------------------
    // Public Properties
    //---------------------------------------------------------------------------------
    // the number of new attachments to add to the list when the user clicks 'Add More'
    public static final Integer NUM_ATTACHMENTS_TO_START = 3;
    public static final Integer NUM_ATTACHMENTS_TO_ADD = 1;
    
    @TestVisible
    private Case objCase;
    
    public string strCaseNumber
    {
        get
        {
            if (objCase.Id != null)
            {
                    Case c = [SELECT CaseNumber FROM Case WHERE Id = :objCase.Id];
                    return c.CaseNumber;
            }
            else
            {
                    return '';
            }
        }
    }
    
    public string strCaseType
    {
        get
        {
            if (objCase.Id != null)
            {
                    return objCase.Type;
            }
            else
            {
                    return '';
            }
        }
    }
    
    public string strReqName 
    {
        get
        {
            if (userInfo.getUserId() != null)
            {
                return userInfo.getName();
            }
            else
            {
                return '';
            }
        }
    }
    
    public string strReqEmail 
    {
        get
        {
            if (userInfo.getUserId() != null)
            {
                return userInfo.getUserEmail();
            }
            else
            {
                return '';
            }
        } 
    }
    
    public string strEventType {get; set;}
    
    public list<SelectOption> lstEventTypes
    {
        get
        {
            list<SelectOption> options = new list<SelectOption>();
            
            Schema.DescribeFieldResult fieldResult = evt__Special_Event__c.evt__Event_Type__c.getDescribe();
            list<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
            
            options.add(new SelectOption('', ''));
            for(Schema.PicklistEntry f : ple)
            {
                options.add(new SelectOption(f.getLabel(), f.getValue()));
            }   
            
            return options;
        }
    }
    
    public list<SelectOption> lstFacilityContacts
    {
        get
        {
            system.debug('*** sgi_EventRequest_Controller | lstFacilityContacts - start');
            
            list<SelectOption> options = new list<SelectOption>();
    		options.add(new SelectOption('', ' '));
            
            system.debug('*** sgi_EventRequest_Controller | lstFacilityContacts - objCase.Host_Facility__c: ' + objCase.Host_Facility__c);
            
            if (objCase.Host_Facility__c != null)
            {
                for(Contact c : [SELECT Id, Name FROM Contact WHERE AccountId = :objCase.Host_Facility__c])
                {
                    options.add(new SelectOption(c.Id, c.Name));
                }   
            }
            
            return options;
        }
    } 
    
    //public evt__Special_Event__c objEvent{get; set;}
    public string strFacilityContact{get; set;}
    
    // list of new attachments to add
    public List<Attachment> newAttachments {get; set;}
    
    //---------------------------------------------------------------------------------
    // Public Methods
    //---------------------------------------------------------------------------------
    public sgi_EventRequest_Controller(ApexPages.StandardController stdController) 
    {
       	objCase = (Case)stdController.getRecord();
        
        // instantiate the list with attachments
        newAttachments = new List<Attachment>();
        for (integer x = 0; x < NUM_ATTACHMENTS_TO_START; x++)
        {
            newAttachments.add(new Attachment());
        }
    }

    // Add more attachments action method
    public void addMore()
    {
        // append NUM_ATTACHMENTS_TO_ADD to the new attachments list
        for (Integer idx = 0; idx < NUM_ATTACHMENTS_TO_ADD; idx++)
        {
            newAttachments.add(new Attachment());
        }
    } 
    
    public pageReference SaveRequest()
    {
        pageReference pr = null;
        
        // Save the new Case record
        objCase.RecordTypeId = strEventType == 'Seminar' ? rtCaseSeminar : rtCaseWorkshop;
        objCase.Type = strEventType;
        objCase.Origin = 'Web (Public)';
        objCase.Reason = 'Seminar/Workshop Request';
        objCase.Priority = 'Medium';
        objCase.Requestor_Name__c = strReqName;
        objCase.Requestor_Email__c = strReqEmail;
        objCase.Host_Facility_Contact__c = strFacilityContact;
        
        insert(objCase);
        
        // Now save the attachments to the case
        List<Attachment> toInsert = new List<Attachment>();
        for (Attachment newAtt : newAttachments)
        {
            if (newAtt.Body != null)
            {
                newAtt.parentId = objCase.Id;
                toInsert.add(newAtt);
            }
        }
        
        insert toInsert;
        
        newAttachments.clear();
        newAttachments.add(new Attachment());
        
        return null;
    }   
}