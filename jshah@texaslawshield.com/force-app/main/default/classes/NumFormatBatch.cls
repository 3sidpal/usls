global class NumFormatBatch implements Database.Batchable<sObject> {
 
    String objectName ;
    List<String> phoneFieldList ;
     
    global NumFormatBatch(String objName , List<String> phnFieldList) {
        objectName = objName;
        phoneFieldList = phnFieldList ;
    }
     
    global Database.QueryLocator start(Database.BatchableContext BC){
        if(objectName.equalsIgnoreCase('lead')) {
            String query = 'select ' +  String.join(phoneFieldList,',')  + ' from ' +  objectName + ' where isConverted = false' ; 
            return Database.getQueryLocator(query);        
        } 
        return Database.getQueryLocator('select ' +  String.join(phoneFieldList,',')  + ' from ' +  objectName );  
    }
    
    global void execute(Database.BatchableContext BC, List<sObject> scope){
        List<sObject> sobjList = new List<sObject>();

        for( sObject sobj : scope ) {     
            boolean addObject = false;
            for(String phoneField : phoneFieldList ) {
                if (sobj.get(phoneField) != null) {
                    String s = String.valueof(sobj.get(phoneField)) ;
                    String newPhoneNumber = '';
                    if(String.isNotBlank(s)) {
                        s = s.replaceAll('[^0-9]', '');
                        if (s.length() == 11 && s.startsWith('1')) {
                            s = s.substring(1);
                        }
                        if (s.length() == 12 && s.startsWith('01')) {
                            s = s.substring(2);
                        }
                        if (s.length() == 13 && s.startsWith('001')) {
                            s = s.substring(3);
                        }
                        if (s.length() == 10) {
                            newPhoneNumber = '('+s.substring(0,3)+') '+s.substring(3,6)+'-'+s.substring(6);
                        } 
                        //else {                    used for numbers which are not from the USA
                        //  newPhoneNumber = s;
                        //}
                    }
                    if(String.isNotBlank(newPhoneNumber)) {
                        addObject = true;
                        sobj.put(phoneField,newPhoneNumber);
                    }
                }
            }
            if (addObject)
                sobjList.add(sobj);
        }
        update sobjList;
    }

    global void finish(Database.BatchableContext BC){
    }       
}