/**  
 * @ Version     Modified Date      Modified By     Description
 * @ ------------------------------------------------------------
 * @     1.0     04-09-2019         Unmesha P       Initial Draft
 * @     1.1     04-06-2019         Unmesha P       Update the Primary field on Account whenever a
 * @                                                record is Created / Modified / Deleted if we 
 * @                                                have any one of the Account's child record 
 * @                                                (Zuora__SubscriptionProductCharge__c) with 
 * @                                                Rate Plan Name as Individual.
 * @     1.3     06-24-2019         Eternus         Code optimization to fix the Apex Heap Limits 
 * @                                                issue
 */

public with sharing class UpdatePrimaryOnAccount {

    /**
     * Check for Active Subscriptions that have been deleted having Individual in them.
     * @param  newSubscriptList - List of new values of Active Subscriptions that are updated
     */
    public static void getallSubscriptions(List<Zuora__SubscriptionProductCharge__c> pNewSubscriptList) {

        Set<ID> accIdSet = new Set<ID>();
        for (Zuora__SubscriptionProductCharge__c sup :pNewSubscriptList) {
            if (sup.Zuora__Account__c != null 
                && sup.Zuora__Subscription__c != null 
                && sup.Zuora__RatePlanName__c != null) {
                accIdSet.add(sup.Zuora__Account__c);
            }
        }

        System.debug('@@ getallSubscriptions ' + accIdSet);

        updateAccounts(accIdSet);

    }//getallSubscriptions ends

    /**
     * Check for Active Subscriptions that have been updated
     * having Individual in them.
     * @param  newSubscriptList - List of new values of Active Subscriptions that are updated
     * @param  oldSubscriptionsMap - Map of old values of Active Subscriptions that are updated
     */
    public static void getallSubscriptionsforUpdate(
     List <Zuora__SubscriptionProductCharge__c> pNewSubscriptList, 
     Map <Id, Zuora__SubscriptionProductCharge__c> pOldSubscriptionsMap) {
        Set <ID> accIdSet = new Set <ID>();

        if (pNewSubscriptList.size() > 0) {

            for (Zuora__SubscriptionProductCharge__c sup :pNewSubscriptList) {

                System.debug('@@ sup ' + sup);
                if (sup.Zuora__Account__c != null 
                    && sup.Zuora__Subscription__c != null 
                    && sup.Zuora__RatePlanName__c != pOldSubscriptionsMap.get(sup.Id).Zuora__RatePlanName__c) {
                    accIdSet.add(sup.Zuora__Account__c);
                }
            }

            System.debug('@@ getallSubscriptionsforUpdate ' + accIdSet);

            updateAccounts(accIdSet);
        }//if

    }//getallSubscriptionsforUpdate ends


    /**
     * Marks Account records as Primary if they have Active Subscription Rate Plans with names 
     * having Individual in them.
     * @param  pAccountIdSet - List of Active Accounts
     * @return List<List<Database.SaveResult> return save results for Account records where the 
     *         flag is updated.
     */
    public static List<Database.SaveResult> updateAccounts(Set<Id> pAccountIdSet){

        if(pAccountIdSet.size() == 0){
            return null;
        }

        String sLabel = '%' + System.Label.Individual + '%';
        List <Account> accListToUpdate = new List <Account>();

        accListToUpdate.addAll(
            [Select Id, Primary__c,
                (Select Id, Zuora__RatePlanName__c 
                 From R00N40000001lGjTEAU 
                 Where Zuora__Subscription__r.Zuora__Status__c = 'Active'
                    AND Zuora__RatePlanName__c LIKE :sLabel)
             From Account
             Where Id IN :pAccountIdSet]
        );

        if(accListToUpdate.size() > 0){

            //Set the Primary flag to true if Active Rate Plan names contain Individual    
            for (Account acc :accListToUpdate) {

                acc.Primary__c = acc.R00N40000001lGjTEAU.size() > 0 ? true : false;
            }//for
            return Database.update(accListToUpdate, true);
        }
        return null;
    }//updateAccounts ends

}//UpdatePrimaryOnAccount ends