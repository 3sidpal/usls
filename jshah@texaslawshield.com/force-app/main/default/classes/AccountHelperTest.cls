@isTest
public without sharing class AccountHelperTest {
    
        //******************************************************************
        //  Link Up Person Accounts to Parent Accounts
        //******************************************************************
	    static testMethod void LinkPersonAccounts() {
	    
	    
	    test.startTest();    
	        // Setup Test Data for Testing   
			TestDataGenerator td = new TestDataGenerator();
			
			//Get Accounts
			List<Account> accountList = td.getAccounts(false);
			List<Account> accountPersonList = new List<Account>();
			Map<String, Id> recordtypesMap = new Map<String, Id>();
			Integer counter=1;	 
		
		
		
		
			insert accountList;
			update accountList;
		
			// Create Map of Person Account Record Types
			for (RecordType rt:[select Id, Name, SObjectType from RecordType where IsActive = true and isPersonType = true and SObjectType = 'Account']){
				recordtypesMap.put(rt.Name, rt.Id);
			}
		
			//Create Generic Account Records and update with Promo Codes		
			for (Account a:accountList){
				a.Promo_Code__c = String.valueOf(counter);

				counter = counter+1;
				
            	system.debug('Update Promo Codes a :'+a);				
			}
			update accountList;
				
			//Create Person Accounts and link	
			for(Integer i = 1; i <= accountList.size(); i++) {
				Account a = new Account();
				a.FirstName = 'Member First Name ' + i;
				a.LastName  = 'Member Last Name ' + i;  
				a.recordTypeId = recordtypesMap.get('Member');
				a.fb_connect__AccountReseller__c = string.valueOf(i);
				//a.BillingState = 'TX';       
				//a.BillingCountry = 'US';   
            	accountPersonList.add(a);
            	
            	system.debug('Record Push in : a :'+a);
            }
            insert accountPersonList;
            update accountPersonList;

			// Now check that the Facility field is connected
			/*
			for (Account pac:[select Id, Facilty__c, fb_connect__AccountReseller__c, Facilty__r.Promo_Code__c from Account Where IspersonAccount = true]){
					system.assertEquals(pac.fb_connect__AccountReseller__c, pac.Facilty__r.Promo_Code__c);
			}
			*/

        test.stopTest();    

	    }	// End of LinkPersonAccounts
	    
}