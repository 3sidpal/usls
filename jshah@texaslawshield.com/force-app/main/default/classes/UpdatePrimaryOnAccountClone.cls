/**
 * @ Version     Modified Date      Modified By     Description
 * @ ------------------------------------------------------------
 * @     1.0     04-09-2019         Unmesha P       Initial Draft
 * @     1.1     04-06-2019         Unmesha P       Update the Primary field on Account whenever a
 * @                                                record is Created / Modified / Deleted if we
 * @                                                have any one of the Account's child record
 * @                                                (Zuora__SubscriptionProductCharge__c) with
 * @                                                Rate Plan Name as Individual.
 * @     1.3     06-24-2019         Eternus         Code optimization to fix the Apex Heap Limits
 * @                                                issue
 */

public with sharing class UpdatePrimaryOnAccountClone {

    /**
     * Check for Active Subscriptions that have been deleted having Individual in them.
     * @param  newSubscriptList - List of new values of Active Subscriptions that are updated
     */
    public static void getallSubscriptions(List<Zuora__SubscriptionProductCharge__c> pNewSubscriptList) {
        //
        Set<Id> accIdSet = new Set<Id>();

        for (Zuora__SubscriptionProductCharge__c supItr : pNewSubscriptList) {
            if (
                supItr.Zuora__Account__c != null
             && supItr.Zuora__Subscription__c != null
             && supItr.Zuora__RatePlanName__c != null
            ) {
                accIdSet.add(supItr.Zuora__Account__c);
            }
        }

        System.debug('@@ getallSubscriptions ' + accIdSet);

        updateAccounts(accIdSet);
        //
    } // getallSubscriptions ends.

    /**
     * Check for Active Subscriptions that have been updated
     * having Individual in them.
     * @param  newSubscriptList - List of new values of Active Subscriptions that are updated
     * @param  oldSubscriptionsMap - Map of old values of Active Subscriptions that are updated
     */
    public static void getallSubscriptionsforUpdate(
        List <Zuora__SubscriptionProductCharge__c> pNewSubscriptList,
        Map <Id, Zuora__SubscriptionProductCharge__c> pOldSubscriptionsMap
    ) {
        //
        System.debug('**delete debug3**'+pNewSubscriptList);
        System.debug('**delete debug4**'+pOldSubscriptionsMap);
        Set<Id> accIdSet = new Set<Id>();

        if (!pNewSubscriptList.isEmpty()) {
            for (Zuora__SubscriptionProductCharge__c supItr : pNewSubscriptList) {

                System.debug('@@ sup ' + supItr);

                if (
                    supItr.Zuora__Account__c != null
                 && supItr.Zuora__Subscription__c != null
                 && supItr.Zuora__RatePlanName__c != pOldSubscriptionsMap.get(supItr.Id).Zuora__RatePlanName__c
                ) {
                    accIdSet.add(supItr.Zuora__Account__c);
                }
            }

            System.debug('@@ getallSubscriptionsforUpdate ' + accIdSet);

            updateAccounts(accIdSet);
        } // if.
        //
    } // getallSubscriptionsforUpdate ends


    /**
     * Marks Account records as Primary if they have Active Subscription Rate Plans with names
     * having Individual in them.
     * @param  pAccountIdSet - List of Active Accounts
     * @return List<List<Database.SaveResult> return save results for Account records where the
     *         flag is updated.
     */
    public static List<Database.SaveResult> updateAccounts(Set<Id> pAccountIdSet) {
        //
        System.debug('**delete debug6**'+pAccountIdSet);

        if(pAccountIdSet.isEmpty()) {
            return null;
        }

        String sLabel = '%' + System.Label.Individual + '%';
        System.debug('**delete debug8**'+sLabel);
        List<Account> accListToUpdate = new List<Account>();

        accListToUpdate.addAll(
            [SELECT Id
                  , Primary__c
                  , ( SELECT Id
                           , Zuora__RatePlanName__c
                        FROM R00N40000001lGjTEAU
                       WHERE Zuora__Subscription__r.Zuora__Status__c = 'Active'
                         AND Zuora__RatePlanName__c LIKE :sLabel
                    )
               FROM Account
              WHERE Id IN :pAccountIdSet
        ]);

        System.debug('***updateAccounts' + accListToUpdate);
        System.debug('**delete debug9**'+accListToUpdate);

        if(!accListToUpdate.isEmpty()) {
            //Set the Primary flag to true if Active Rate Plan names contain Individual
            for (Account accItr : accListToUpdate) {
                accItr.Primary__c = !accItr.R00N40000001lGjTEAU.isEmpty()
                                  ? true
                                  : false;
            } // for

            return Database.update(accListToUpdate, true);
        }
        return null;
        //
    } // updateAccounts ends.

}//UpdatePrimaryOnAccount ends