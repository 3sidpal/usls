trigger Account_Trigger on Account (before insert, before update) {
		
        // ************************************************************************************
        // Set up variables for trigger work
        // ************************************************************************************

        //List<Account> childObjectList;   // Used to store Tasks to process
        //childObjectList = Trigger.new;
        
 			// Process Person Accounts Only
			List<Account> accountPersonRecs = new List<Account>(); 
			// List of Accounts to update        
			List<Account> accountUpdate = new List<Account>();
			// Set of Reseller Values to query on Business Accounts
			Set<string> accountResellerSet = new Set<String>(); 
			// Create Map of Promo Codes and Business Accounts
			Map<string, Id> accountBusinessMap = new Map<string, Id>();          
        
        // ************************************************************************************
		// Run for Before CODE
        // ************************************************************************************	
        try {	
	        if (
	                 (!RecursiveTriggerHelper.isAlreadyModified() || Test.isRunningTest()) 
	            ){
	            RecursiveTriggerHelper.setAlreadyModified();
						
	 					// ************************************************************************************
			            // Call up the Person Account "AccountReseller" field lookup code
			            // If Account Reseller is not Empty do this 
			            // ************************************************************************************ 
	        
					    for(Account a: Trigger.new){
					    	
					    	// Collect and Create the Records to Query and Records to Process 
					    	if (a.IsPersonAccount && a.fb_connect__AccountReseller__c !=''){
					    		accountResellerSet.add(a.fb_connect__AccountReseller__c);
					    	}
					    }
					    
					    // If there are records to perform a lookup then do it
					    if (accountResellerSet.size() > 0){
						    	// Get Business Accounts and create map for linking
						    	for (Account ba:[select Id, Promo_Code__c from Account where Promo_Code__c IN :accountResellerSet and IsPersonAccount = false and Promo_Code__c !='']){
						    		accountBusinessMap.put(ba.Promo_Code__c, ba.Id);
						    	}
				
							    // Now go and update the Person Account Facility field with correct connection to Parent Account
							    for (Account ua:Trigger.new){
							    	// If record is person account and there is a match for Reseller then process update of Facility Field and Reseller field is not Empty, Process
				 					if(ua.IsPersonAccount && accountBusinessMap.get(ua.fb_connect__AccountReseller__c) != null && ua.fb_connect__AccountReseller__c != null){
				 						ua.Facilty__c = accountBusinessMap.get(ua.fb_connect__AccountReseller__c);
				 					}
							    } 
					    }
	        	
	            }// End of RecursiveTriggerHelper
        } catch(exception e){
           	system.debug('*** Account Update Error - Trying to Update Facilty value : '+e);
        } 
}// End of Account_Trigger