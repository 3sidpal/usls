trigger Taskdeletion on Task (before delete)
{
   String ProfileId = UserInfo.getProfileId();  
   List<Profile> profiles=[select id from Profile where name='*TLS: Members Services Rep' or name='*TLS: Member Services Management'];

   if (2!=profiles.size())
   {
    // unable to get the profiles - handle error
   }
   else
   {
       for (Task a : Trigger.old)      
       {            
          if ( (profileId==profiles[0].id) || (profileId==profiles[1].id) )
          {
             a.addError('You cannot delete this record');
          }
       }            
   }
}