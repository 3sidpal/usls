trigger subscriptionProductChargeTriggerClone on Zuora__SubscriptionProductCharge__c (
    before insert,
    before update,
    before delete,
    after insert,
    after update,
    after delete,
    after undelete
) {
    TriggerDispatcher.run(new SubscriptionProductChargeTriggerHandler(), Trigger.operationType);
}