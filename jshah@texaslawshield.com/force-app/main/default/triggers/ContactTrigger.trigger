/**  
* ─────────────────────────────────────────────────────────────────────────────────────────────────┐
* @author         Unmesha Punyamurthula
* @version        1.0
* @created        06/04/2019
* @TestClass      AfterContactEmailUpdateTest
* ─────────────────────────────────────────────────────────────────────────────────────────────────┘
*/



trigger ContactTrigger on Contact (before insert, before update, after insert) {
    
   // This is a custom meta data used for deactivating and activating the trigger in Production
    Trigger_Settings__mdt TriggerFlag = [SELECT 
                                         Label, isActive__c 
                                         FROM Trigger_Settings__mdt where Label = 'TLS_ContactTrigger' limit 1];
    
    if(TriggerFlag.isActive__c) {  // Check if trigger is active    
        if(Trigger.isbefore){ // check if event is before
            AfterContactEmailUpdate.beforeUpDateMethod(Trigger.new,Trigger.OldMap);    // call our apex class 
        }
    }
}