/*
 * @ Author: Tharun G
 * @ Objects Referenced: Account, SubscriptionProductCharge__c
 * @ Description: This trigger is used to calculate Subsription type of member
 * @ @Annual/Monthly/Diamond/Other) based up on the name of the Subscription 
 * @ Charge Name filed on  Zuora__SubscriptionProductCharge__c Object.
 *
 * @ Version     Modified Date      Modified By     Description
 * @ ------------------------------------------------------------
 * @     1.0     06-30-2018         Tharun G        Initial Draft
 * @     1.1     04-06-2019         Tharun G        This is calling another helper menthod to 
 * @                                                update on Primary on Account
 * @     1.2     06-14-2019         Unmesha P       Modified code to fix the Apex Heap Limits issue
 * @     1.3     06-24-2019         Eternus         Code optimization to fix the Apex Heap Limits 
 *                                                  issue
 */ 

trigger subscriptionProductChargeTrigger on Zuora__SubscriptionProductCharge__c (after insert, 
                                                                                 after update, 
                                                                                 before delete, 
                                                                                 after delete) {

    //Before Logic
    if(trigger.isBefore){

        //isDelete
        if(trigger.isDelete){

            System.debug('**Old code Block 1 begins - ' + Datetime.now());

            set<id> subID=new set<id>();
            set<id> accountID=new set<id>();
            Map<id,string> acc_subscriptionsMap=new Map<id,string>();

            for(Zuora__SubscriptionProductCharge__c subs:Trigger.old){
                
                if(subs.Zuora__Account__c != null){
                    acc_subscriptionsMap.put(subs.Zuora__Account__c,subs.name);
                    subID.add(subs.id);
                    accountID.add(subs.Zuora__Account__c);
                }
            }//for

            if(acc_subscriptionsMap.size() > 0){
                subscriptionProductChargeHelper.getallsubscriptions(accountID, true, subID);
            }

            System.debug('**Old code Block 1 ends - ' + Datetime.now());

        }//isDelete
    }//isBefore


    //After Logic
    if(trigger.isAfter){

        System.debug('**Old code Block 2 begins - ' + Datetime.now());

        //Insert logic
        if(trigger.isInsert){
            UpdatePrimaryOnAccount.getallSubscriptions(trigger.new);
            System.debug('@@Product Trigger ecxcuted');
        }

        //Update logic
        if(trigger.isUpdate){
            List<Account> parentAccount = new list<Account>();
            Set<id> setAccountId = new Set<id>();
            Map<id,string> subscriptionMap = new Map<id,string>();
            Map<id,Account> accountMap = new Map<id,Account>();
            Map<id,string> acc_subscriptionsMap=new Map<id,string>();
            Set<id> accountId=new set<id>();
            List<Zuora__SubscriptionProductCharge__c> sub_list = new list<Zuora__SubscriptionProductCharge__c>();

            for (Zuora__SubscriptionProductCharge__c sub : Trigger.new){
                if(sub.Zuora__Account__c != null){
                    setAccountId.add(sub.Zuora__Account__c);   
                }
            }
            
            for(Account Acc:[select id, member_type__c from Account where id IN :setAccountId]){
                accountMap.put(acc.id,acc);
            }
            
            List<Account> updateAccounts = new List<Account>();
            Map<id,account> updateAccMAp = new Map<id,account>();

            for(Zuora__SubscriptionProductCharge__c subs :Trigger.new){

                if(subs.Zuora__Account__c != null){

                    if(Subs.name.contains('Annual')){
                        account acc=accountMap.get(subs.Zuora__Account__c);
                        acc.member_type__c ='Annual';
                        updateAccounts.add(acc);
                        updateAccMAp.put(acc.id,acc);
                    }
                    else if(Subs.name.contains('Monthly')){
                        account acc1=accountMap.get(subs.Zuora__Account__c);     
                        acc1.member_type__c ='Monthly';
                        updateAccounts.add(acc1);
                        updateAccMAp.put(acc1.id,acc1);
                    }
                    else if(Subs.name.contains('Diamond')){
                        account acc2=accountMap.get(subs.Zuora__Account__c);   
                        acc2.member_type__c ='Diamond';
                        updateAccounts.add(acc2);
                        updateAccMAp.put(acc2.id,acc2);
                    }
                    else{
                        //acc_subscriptionsMap.put(subs.Zuora__Account__c, subs.name);
                        accountId.add(subs.Zuora__Account__c);
                    }
                }
            }//for

            if(updateAccounts.size() > 0){
                //update updateAccounts;
                update updateAccMAp.values();
            } 
            
            if(acc_subscriptionsMap.size() > 0){
                subscriptionProductChargeHelper.getallsubscriptions(accountId, false, null);
            }
            System.debug('**Old code Block 2 ends - ' + Datetime.now());

            System.debug('**New code Block 3 begins - ' + Datetime.now());
            System.debug('**delete debug1** '+trigger.new);
            System.debug('**delete debug2** '+trigger.oldMap);
            UpdatePrimaryOnAccount.getallSubscriptionsforUpdate(trigger.new, trigger.oldMap);
            System.debug('**New code Block 3 ends - ' + Datetime.now());

        }//isUpdate ends

        //Delete logic
        if(trigger.isDelete){
            UpdatePrimaryOnAccount.getallSubscriptions(trigger.old);
        }//isDelete ends

    }//isAfter ends

}//subscriptionProductChargeTrigger ends