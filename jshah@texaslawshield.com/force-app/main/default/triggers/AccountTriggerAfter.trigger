trigger AccountTriggerAfter on Account (after insert, after delete) {
    
    Trigger_Settings__mdt TriggerFlag = [SELECT 
                                         Label, isActive__c 
                                         FROM Trigger_Settings__mdt where Label = 'TLS_AccountTriggerAfter' limit 1];
    if(TriggerFlag.isActive__c) {                                     
        
        if(Trigger.isafter && !Trigger.isDelete){
            MatchAccountEmailHandler.findDuplicateEmail(trigger.new,trigger.OldMap);
            //MatchAccountEmailHandlerPrimary.findDuplicateEmail(trigger.new,trigger.OldMap);
        }
        
        if(Trigger.isDelete){
            //system.debug(trigger.old);
            MatchAccountEmailHandler.findDuplicateEmail(trigger.old,trigger.OldMap);
            // MatchAccountEmailHandlerPrimary.findDuplicateEmail(trigger.old,trigger.OldMap);
            
            //System.debug('@@in beforetrigger');
            
        }
    }
    
}